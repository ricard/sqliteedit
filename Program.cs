using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace SQLiteEdit
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			if(args.Length > 0)
			{
				List<string> files = new List<string>();
				for(int i = 0; i < args.Length; i++)
				{
					if(File.Exists(args[i]))
						files.Add(args[i]);
				}
				Application.Run(new MainForm(files.ToArray()));
			}
			else
				Application.Run(new MainForm());
		}
	}
}