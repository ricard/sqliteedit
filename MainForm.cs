using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using SQLiteEdit.Properties;

namespace SQLiteEdit
{
	public partial class MainForm : Form
	{
		private Cache _dataCache;
		private string _displayedDatabase = "";
		private string _displayedTable = "";
		private DbConnection _currentCnx;
		private readonly List<string> _fileToLoad = new List<string>();
		private object[] _currentEditedRow = null;

		private readonly Dictionary<string, SQLiteConnection> _cnxs = new Dictionary<string, SQLiteConnection>();
		private bool _newRowNeeded = false;

		public MainForm()
		{
			InitializeComponent();
//			if (WindowState == FormWindowState.Normal)
			ClientSize = Settings.Default.mainFormClientSize;
			//else 
			//  this.De
			Debug.Listeners.Add(new MyTraceListener(outputRichTextBox));
		}

		public MainForm(IEnumerable<string> fileToLoad)
			: this()
		{
			_fileToLoad.AddRange(fileToLoad);
		}

		public void ClearTabs()
		{
			currentTableLabel.Text = "";
			currentDatabaseLabel.Text = "";
			currentSchemaLabel.Text = "";
			currentQueryLabel.Text = "";
			rowsCountStatusLabel.Text = "";
			timeStatusLabel.Text = "";
			dataGridView.Rows.Clear();
			dataGridView.Columns.Clear();
			tablesDataGridView.Rows.Clear();
			schemaGridView.Rows.Clear();
			settingsDataGridView.Rows.Clear();
			createQueryTextBox.Text = "";
			executeQueryToolStripButton.Enabled = false;
			_displayedDatabase = "";
			_displayedTable = "";
			_currentCnx = null;
		}

		private void WriteToOutput(string msg)
		{
			outputRichTextBox.AppendText(msg);
			outputRichTextBox.SelectionStart = outputRichTextBox.Text.Length;
			outputRichTextBox.ScrollToCaret();
		}

		#region Database management

		public void OpenDatabase(string file)
		{
			if (!IsManaged(file))
				AddDbFile(file);

			if (!IsOpended(file))
			{
				OpenConnection(file);
			}
		}

		/// <summary>
		/// Returns true if the specified file is currectly managed by SQLiteEdit.
		/// </summary>
		/// <param name="filename">SQLite database file</param>
		/// <returns>True if the file is managed (i.e. present into the current connection list).</returns>
		public bool IsManaged(string filename)
		{
			return Settings.Default.dbFiles.Contains(filename);
		}

		/// <summary>
		/// Returns true if this databse already have an opened connection.
		/// </summary>
		/// <param name="filename">SQLite database file</param>
		/// <returns>True if the connexion is currently open</returns>
		public bool IsOpended(string filename)
		{
			return _cnxs.ContainsKey(filename);
		}

		/// <summary>
		/// Returns the DbConnection object for the specified db file.
		/// If the connection isn't already opened, thant create and open it.
		/// </summary>
		/// <param name="filename">SQLite database file</param>
		/// <returns>DbConnection object</returns>
		public SQLiteConnection OpenConnection(string filename)
		{
			if (!IsManaged(filename))
				AddDbFile(filename);
			if (!IsOpended(filename))
			{
				SQLiteConnection cnx = new SQLiteConnection("Data Source=" + filename);
				cnx.Open();
				TreeNode[] nodes = dbTreeView.TopNode.Nodes.Find(filename, false);
				dbTreeView.BeginUpdate();
				nodes[0].ImageKey = "Database.png";
				nodes[0].SelectedImageKey = "Database.png";
				ReadDbContent(cnx, nodes[0]);
				dbTreeView.Sort();
				dbTreeView.EndUpdate();
				_cnxs[filename] = cnx;
				versionStatusLabel.Text = "SQLite v" + cnx.ServerVersion;
			}
			return _cnxs[filename];
		}

		public void CloseConnection(string filename)
		{
			if (IsOpended(filename))
			{
				DbConnection cnx = _cnxs[filename];
				if (cnx.ConnectionString == _displayedDatabase)
					ClearTabs();
				_cnxs.Remove(filename);
				cnx.Dispose();
				TreeNode[] nodes = dbTreeView.TopNode.Nodes.Find(filename, false);
				nodes[0].ImageKey = "DatabaseOffline.png";
				nodes[0].SelectedImageKey = "DatabaseOffline.png";
				nodes[0].Nodes.Clear();
				versionStatusLabel.Text = "Not Connected";
				ClearTabs();
			}
		}

		public void AddDbFile(string filename)
		{
			if (!IsManaged(filename))
			{
				Settings.Default.dbFiles.Add(filename);
				Settings.Default.Save();
				AddDbToTree(filename);
			}
		}

		private void AddDbToTree(string filename)
		{
			TreeNode node = dbTreeView.TopNode.Nodes.Add(filename, Path.GetFileName(filename));
			node.ImageKey = "DatabaseOffline.png";
			node.SelectedImageKey = "DatabaseOffline.png";
			node.ToolTipText = filename;
			node.Tag = filename;
		}

		private DbConnection GetConnection(TreeNode node)
		{
			while (node.Level > 1)
				node = node.Parent;
			if (_cnxs.ContainsKey(node.Name))
				return _cnxs[node.Name];
			else
				return null;
		}

		private void ConnectToSelectedDb()
		{
			TreeNode node = dbTreeView.SelectedNode;
			if (node.Level == 1)
			{
				string filename = node.Name;
				DbConnection cnx = OpenConnection(filename);
				node.Expand();
				UpdateToolBarStates(node);
				FillDatabaseTab(cnx);
				FillQueryTab(cnx);
			}
		}

		private void ReadDbContent(DbConnection cnx, TreeNode parent)
		{
			DataTable schema = cnx.GetSchema("tables");
			foreach (DataRow row in schema.Rows)
			{
				AddTable(cnx, parent, (string)row["TABLE_NAME"]);
			}
			AddTable(cnx, parent, "sqlite_master");
			schema = cnx.GetSchema("views");
			foreach (DataRow row in schema.Rows)
			{
				AddView(cnx, parent, (string)row["TABLE_NAME"]);
			}
		}

		private static void AddTable(DbConnection cnx, TreeNode dbNode, string tableName)
		{
			TreeNode[] nodes = dbNode.Nodes.Find("tables", false);
			TreeNode tablesNode;
			if (nodes.Length > 0)
				tablesNode = nodes[0];
			else
			{
				tablesNode = dbNode.Nodes.Add("tables", "Tables", "Tables.png", "Tables.png");
			}
			if(tableName.StartsWith("sqlite_"))
				tablesNode.Nodes.Add(tableName, tableName, "SystemTable.png", "SystemTable.png");
			else
				tablesNode.Nodes.Add(tableName, tableName, "Table.png", "Table.png");
		}

		private static void AddView(DbConnection cnx, TreeNode dbNode, string name)
		{
			TreeNode[] nodes = dbNode.Nodes.Find("views", false);
			TreeNode viewsNode;
			if (nodes.Length > 0)
				viewsNode = nodes[0];
			else
			{
				viewsNode = dbNode.Nodes.Add("views", "Views", "Views.png", "Views.png");
			}
			viewsNode.Nodes.Add(name, name, "View.png", "View.png");
		}

		private void UpdateToolBarStates(TreeNode node)
		{
			if (node == null)
			{
				connectToolStripButton.Enabled = false;
				disconnectToolStripButton.Enabled = false;
				addTableToolStripButton.Enabled = false;
				dropTableToolStripButton.Enabled = false;
				addViewToolStripButton.Enabled = false;
				dropViewToolStripButton.Enabled = false;
			}
			else if (node.Level == 1)
			{
				bool opened = IsOpended(node.Name);
				connectToolStripButton.Enabled = !opened;
				disconnectToolStripButton.Enabled = opened;
				addTableToolStripButton.Enabled = opened;
				dropTableToolStripButton.Enabled = false;
				addViewToolStripButton.Enabled = opened;
				dropViewToolStripButton.Enabled = false;
			}
			else if (node.Level == 2)
			{
				bool isTables = node.Name == "tables";
				bool isViews = node.Name == "views";
				connectToolStripButton.Enabled = false;
				disconnectToolStripButton.Enabled = false;
				addTableToolStripButton.Enabled = isTables;
				dropTableToolStripButton.Enabled = false;
				addViewToolStripButton.Enabled = isViews;
				dropViewToolStripButton.Enabled = false;
			}
			else if (node.Level == 3) // table or view
			{
				bool isTable = node.Parent.Name == "tables";
				bool isView = node.Parent.Name == "views";
				connectToolStripButton.Enabled = false;
				disconnectToolStripButton.Enabled = false;
				addTableToolStripButton.Enabled = isTable;
				dropTableToolStripButton.Enabled = isTable;
				addViewToolStripButton.Enabled = isView;
				dropViewToolStripButton.Enabled = isView;
			}
			else
			{
				connectToolStripButton.Enabled = false;
				disconnectToolStripButton.Enabled = false;
				addTableToolStripButton.Enabled = false;
				dropTableToolStripButton.Enabled = false;
				addViewToolStripButton.Enabled = false;
				dropViewToolStripButton.Enabled = false;
			}
		}

		private void FillDatabaseTab(DbConnection cnx)
		{
			_currentCnx = cnx;
			tablesDataGridView.Rows.Clear();
			int tablesCount = 0;
			int viewsCount = 0;
			using (DbCommand cmd = cnx.CreateCommand(),
											 cmd2 = cnx.CreateCommand())
			{
				cmd.CommandText = "SELECT * from sqlite_master";
				using (DbDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						string type = (string)reader[0];
						Image img;
						if (type == "table")
						{
							tablesCount++;
							img = imageList.Images["Table.png"];
						}
						else if (type == "view")
						{
							viewsCount++;
							img = imageList.Images["View.png"];
						}
						else
							continue;
						int idx = tablesDataGridView.Rows.Add();
						DataGridViewRow row = tablesDataGridView.Rows[idx];
						row.Cells[0].Value = img;
						row.Cells[1].Value = reader[1];
						cmd2.CommandText = string.Format("SELECT count(*) FROM '{0}'", reader[2]);
						row.Cells[2].Value = cmd2.ExecuteScalar();
						row.Cells[3].Value = reader[4];
					}
				}
			}
			currentDatabaseLabel.Text =
				string.Format("Database {0}: {1} table(s) / {2} view(s)", cnx.DataSource, tablesCount, viewsCount);
		}

		#endregion

		#region Data management

		private void RefreshDataGrid(DbConnection cnx, string tableName)
		{
			// Create a DataRetriever and use it to create a Cache object
			// and to initialize the DataGridView columns and rows.
			_displayedDatabase = cnx.ConnectionString;
			_displayedTable = tableName;
			currentTableLabel.Text = cnx.DataSource + " : " + tableName;
			try
			{
				DateTime t0 = DateTime.Now;
				_dataCache = CreateDataCache(cnx, tableName);
				UpdateColumns();
				UpdateRowsCount();
				TimeSpan t = DateTime.Now.Subtract(t0);
				timeStatusLabel.Text = String.Format("Time: {0:N1} ms", t.TotalMilliseconds);
				rowsCountStatusLabel.Text = String.Format("{0:N0} rows", _dataCache.RowCount);
			}
			catch (SqlException)
			{
				MessageBox.Show("Connection could not be established. " +
						"Verify that the connection string is valid.");
				Application.Exit();
			}
		}

		private void UpdateRowsCount()
		{
			if (_dataCache.RowCount == 0)
				this.dataGridView.Rows.Clear();
			else
				this.dataGridView.RowCount = _dataCache.RowCount + (dataGridView.AllowUserToAddRows ? 1 : 0);
		}

		private void UpdateColumns()
		{
			dataGridView.Columns.Clear();
			foreach (DataColumn column in _dataCache.Columns)
			{
				dataGridView.Columns.Add(
					column.ColumnName, column.ColumnName);
			}
			dataGridView.Columns[0].Visible = false;	// ROWID
		}

		private Cache CreateDataCache(DbConnection cnx, string tableName)
		{
			DataRetriever retriever =
				new DataRetriever(cnx, tableName, "");
			return new Cache(retriever, 200);
		}

        private void dataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (_newRowNeeded && e.RowIndex == _dataCache.RowCount)
            {
            }
            else if (_currentEditedRow != null
                     && e.RowIndex == dataGridView.CurrentRow.Index)
            {

            }
            else
            {
                object val = _dataCache.RetrieveElement(e.RowIndex, e.ColumnIndex);
                //Console.WriteLine("Formating: type = '{0}' for value '{1}'", val.GetType(), val);
                if (val == DBNull.Value)
                    e.CellStyle.BackColor = Color.Yellow;
                else
                    e.CellStyle.BackColor = Color.White;
            }
        }

        private void dataGridView_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
		{
			if (_newRowNeeded && e.RowIndex == _dataCache.RowCount)
			{
				if (dataGridView.IsCurrentCellInEditMode)
				{
					e.Value = "<?>";
				}
				else if (_currentEditedRow != null)
					e.Value = _currentEditedRow[e.ColumnIndex];
				else
				{
					// Show a blank value if the cursor is just resting
					// on the last row.
					e.Value = string.Empty;
				}
			}
			else if (_currentEditedRow != null 
			         && e.RowIndex == dataGridView.CurrentRow.Index
			         && _currentEditedRow[e.ColumnIndex] != null)
					e.Value = _currentEditedRow[e.ColumnIndex];
			else
				e.Value = _dataCache.RetrieveElement(e.RowIndex, e.ColumnIndex);
		}

		private void dataGridView_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
		{
			DataGridViewCell currentCell = dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];
			currentCell.Tag = true;
			if (_currentEditedRow == null)
			{
				_currentEditedRow = new object[dataGridView.Columns.Count];
				//foreach (DataGridViewCell cell in dataGridView.Rows[e.RowIndex].Cells)
				//{
				//  _currentEditedRow[cell.ColumnIndex] = cell.Value;
				//}
			}
			_currentEditedRow[e.ColumnIndex] = e.Value;
		}

		private void dataGridView_UserAddedRow(object sender, DataGridViewRowEventArgs e)
		{
		}

		private void dataGridView_NewRowNeeded(object sender, DataGridViewRowEventArgs e)
		{
			_newRowNeeded = true;
		}

		private void dataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
		{
		}

		private void dataGridView_RowLeave(object sender, DataGridViewCellEventArgs e)
		{
			if(_newRowNeeded && !dataGridView.IsCurrentRowDirty)
				_newRowNeeded = false;
		}

		private void dataGridView_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (_currentEditedRow != null)
			{
				try
				{
					DataGridViewRow row = dataGridView.Rows[e.RowIndex];
					int rowIndex = e.RowIndex;
					if (_newRowNeeded)
					{
						_newRowNeeded = false;
						System.Text.StringBuilder sql = new System.Text.StringBuilder();
						using (DbCommand cmd = _currentCnx.CreateCommand())
						{
							sql.AppendFormat("INSERT INTO {0} (", _displayedTable);
							bool first = true;
							foreach (DataGridViewColumn col in dataGridView.Columns)
							{
								if (_currentEditedRow[col.Index] != null)
								{
									if (first)
										first = false;
									else
										sql.Append(", ");
									sql.AppendFormat("[{0}]", col.Name);
								}
							}
							sql.Append(") VALUES (");
							first = true;
							foreach (DataGridViewCell cell in row.Cells)
							{
								if (_currentEditedRow[cell.ColumnIndex] != null)
								{
									if (first)
										first = false;
									else
										sql.Append(", ");
									//sql.AppendFormat("{0}", cell.Value);
									sql.Append("?");
									DbParameter param = cmd.CreateParameter();
									cmd.Parameters.Add(param);
									param.Value = cell.Value;
									cell.Tag = null;
								}
							}
							sql.Append(")");
							cmd.CommandText = sql.ToString();
							cmd.ExecuteNonQuery();
						}
						//string sql = cmd.CommandText.Replace("?", value.ToString());
						_dataCache = CreateDataCache(_currentCnx, _displayedTable);
						dataGridView.Invalidate();
						System.Text.StringBuilder outputMessage = new System.Text.StringBuilder();
						outputMessage.AppendLine("Query: " + sql.ToString());
						outputMessage.AppendFormat("Row {0} on {1} successfully inserted...",
																					dataGridView.Rows[rowIndex].Cells[0].Value,
																					_displayedTable);
						outputMessage.AppendLine();
						WriteToOutput(outputMessage.ToString());
					}
					else
					{
						System.Text.StringBuilder sql = new System.Text.StringBuilder();
						using (DbCommand cmd = _currentCnx.CreateCommand())
						{
							sql.AppendFormat("UPDATE {0} SET ", _displayedTable);
							bool first = true;
							foreach (DataGridViewCell cell in row.Cells)
							{
								if (_currentEditedRow[cell.ColumnIndex] != null)
								{
									if (first)
										first = false;
									else
										sql.Append(", ");
									//sql.AppendFormat("{0}", cell.Value);
									sql.AppendFormat("[{0}] = ?", dataGridView.Columns[cell.ColumnIndex].Name);
									DbParameter param = cmd.CreateParameter();
									cmd.Parameters.Add(param);
									param.Value = cell.Value;
									cell.Tag = null;
								}
							}
							sql.AppendFormat(" WHERE ROWID = {0}", row.Cells[0].Value);
							cmd.CommandText = sql.ToString();
							cmd.ExecuteNonQuery();
						}
						_dataCache = CreateDataCache(_currentCnx, _displayedTable);
						dataGridView.Invalidate();
						System.Text.StringBuilder outputMessage = new System.Text.StringBuilder();
						outputMessage.AppendLine("Query: " + sql.ToString());
						outputMessage.AppendFormat("Row {0} on {1} successfully updated...",
																					row.Cells[0].Value,
																					_displayedTable);
						outputMessage.AppendLine();
						WriteToOutput(outputMessage.ToString());
					}
				}
				catch (SQLiteException ex)
				{
					MessageBox.Show(ex.Message, "SQLite error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					Debug.WriteLine(ex);
					e.Cancel = true;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "General error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					Debug.WriteLine(ex);
					e.Cancel = true;
				}
				finally
				{
					_currentEditedRow = null;
				}
			}
		}

		private void dataGridView_CancelRowEdit(object sender, QuestionEventArgs e)
		{
			_currentEditedRow = null;
		}

		private void dataGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			System.Text.StringBuilder sql = new System.Text.StringBuilder();
			object rowid = e.Row.Cells[0].Value;
			using (DbCommand cmd = _currentCnx.CreateCommand())
			{
				sql.AppendFormat("DELETE FROM {0} WHERE ROWID = {1}", 
				                 _displayedTable, rowid);
				cmd.CommandText = sql.ToString();
				cmd.ExecuteNonQuery();
			}
			_dataCache = CreateDataCache(_currentCnx, _displayedTable);
			dataGridView.Invalidate();
			System.Text.StringBuilder outputMessage = new System.Text.StringBuilder();
			outputMessage.AppendLine("Query: " + sql.ToString());
			outputMessage.AppendFormat("Row {0} on {1} successfully deleted...",
																		rowid,
																		_displayedTable);
			outputMessage.AppendLine();
			WriteToOutput(outputMessage.ToString());

		}

		#endregion

		#region Schema management

		private void FillSchema(DbConnection cnx, string tableName)
		{
			schemaGridView.Rows.Clear();
			currentSchemaLabel.Text = cnx.DataSource + " : " + tableName;
			using (DbCommand cmd = cnx.CreateCommand())
			{
				cmd.CommandText = "PRAGMA table_info(" + tableName + ")";
				using (DbDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						bool unique = false, notNull = false;
						int idx = schemaGridView.Rows.Add();
						DataGridViewRow row = schemaGridView.Rows[idx];
						if ((Int64)reader[5] == 1)
						{
							row.Cells[0].Value = imageList.Images["ColumnKey.png"];
							unique = true;
							notNull = true;
						}
						else
						{
							string type = ((string)reader[2]).ToUpper();
							if (type == "INTEGER")
								row.Cells[0].Value = imageList.Images["ColumnInt.png"];
							else if (type == "DOUBLE")
								row.Cells[0].Value = imageList.Images["ColumnDouble.png"];
							else if (type == "BLOB")
								row.Cells[0].Value = imageList.Images["ColumnBlob.png"];
							else //if (type == "TEXT")
								row.Cells[0].Value = imageList.Images["ColumnText.png"];
						}
						row.Cells[1].Value = reader[1];
						DataGridViewComboBoxCell comboCell = (DataGridViewComboBoxCell)row.Cells[2];
						if (!comboCell.Items.Contains(reader[2]))
							comboCell.Items.Add(reader[2]);
						row.Cells[2].Value = reader[2];
						row.Cells[3].Value = notNull ? true : reader[3];
						row.Cells[4].Value = unique;
						row.Cells[5].Value = reader[4];
					}
				}
				cmd.CommandText = string.Format("SELECT sql FROM sqlite_master WHERE name = '{0}'", tableName);
				string sql = (string)cmd.ExecuteScalar();
				createQueryTextBox.Text = sql;
			}
		}

		private void schemaGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			Debug.WriteLine(string.Format("Data error atline {0}, column {1}: {2}", e.RowIndex, e.ColumnIndex, e.Exception.Message), "Debug");
		}

		private void schemaSplitContainer_SplitterMoved(object sender, SplitterEventArgs e)
		{
			SQLiteEdit.Properties.Settings.Default.schemaSplitterDistance = schemaSplitContainer.SplitterDistance;
		}

		#endregion

		#region Query tab

		private void FillQueryTab(DbConnection cnx)
		{
			currentQueryLabel.Text =
				string.Format("Database {0}", cnx.DataSource);
			_currentCnx = cnx;
			executeQueryToolStripButton.Enabled = true;
		}

		private void AddToHistory(string sql)
		{
			if (SQLiteEdit.Properties.Settings.Default.queryHistory == null)
				SQLiteEdit.Properties.Settings.Default.queryHistory = new StringCollection();
			StringCollection history = SQLiteEdit.Properties.Settings.Default.queryHistory;
			if (history.Contains(sql))
			{
				if (history.IndexOf(sql) == 0)
					return;
				history.Remove(sql);
			}
			history.Insert(0, sql);
			while (history.Count > 100)
				history.RemoveAt(100);
			SQLiteEdit.Properties.Settings.Default.Save();
			RefreshHistory();
		}

		private void RefreshHistory()
		{
			Settings settings = Settings.Default;
			historyListBox.Items.Clear();
			if (settings.queryHistory == null)
				return;
			foreach (string s in settings.queryHistory)
			{
				historyListBox.Items.Add(s);
			}

			mainSplitContainer.SplitterDistance = settings.bottomSplitterDistance;
			topSplitContainer.SplitterDistance = settings.topSplitterDistance;
			schemaSplitContainer.SplitterDistance = settings.schemaSplitterDistance;
			querySplitContainer.SplitterDistance = settings.querySplitterDistance;
		}

		private void clearQueryToolStripButton_Click(object sender, EventArgs e)
		{
			queryTextEditorControl.Text = "";
			queryTextEditorControl.Refresh();
		}

		private void executeQueryToolStripButton_Click(object sender, EventArgs e)
		{
			try
			{
				Cursor.Current = Cursors.WaitCursor;
				string sql = queryTextEditorControl.Text;
				DateTime t0 = DateTime.Now;
				queryGridView.Rows.Clear();
				queryGridView.Columns.Clear();
				DbConnection cnx = _currentCnx;
				if (cnx == null)
				{
					MessageBox.Show("Connection to database not opened!\nConnect first to a database before trying to execute a query.",
													"No database connection.", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				long count = 0;
				using (DbCommand cmd = cnx.CreateCommand())
				{
					cmd.CommandText = sql;
					using (DbDataReader reader = cmd.ExecuteReader())
					{
						//DataTable schema = reader.GetSchemaTable();
						for (int i = 0; i < reader.FieldCount; i++)
							queryGridView.Columns.Add(
								reader.GetName(i), reader.GetName(i));
						if (reader.FieldCount > 0)
						{
							object[] values = new object[reader.FieldCount];
							while (reader.Read())
							{
								count++;
								reader.GetValues(values);
								int index = queryGridView.Rows.Add(values);
								DataGridViewRow row = queryGridView.Rows[index];
							    int i = 0;
                                foreach(object val in values)
                                {
                                    if (val is DBNull)
                                        row.Cells[i++].Style.BackColor = Color.Yellow;
                                    else
										row.Cells[i++].Style.BackColor = Color.White;
                                }
							}
						}
					}
				}
				TimeSpan t = DateTime.Now.Subtract(t0);
				timeStatusLabel.Text = String.Format("Time: {0:N1} ms", t.TotalMilliseconds);
				rowsCountStatusLabel.Text = String.Format("{0:N0} rows", count);
				AddToHistory(sql); // Only if suceeded
			}
			catch (SQLiteException ex)
			{
				MessageBox.Show(ex.Message, "SQLite error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Debug.WriteLine(ex);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "General error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Debug.WriteLine(ex);
			}
			finally
			{
				Cursor.Current = Cursors.Default;
			}
		}

		private void loadQueryToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (openQueryDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
			{
				string file = openQueryDialog.FileName;
				queryTextEditorControl.LoadFile(file);
			}
		}

		private void saveQueryToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (saveQueryDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
			{
				string file = saveQueryDialog.FileName;
				queryTextEditorControl.SaveFile(file);
			}

		}

		private void historyListBox_DoubleClick(object sender, EventArgs e)
		{
			string sql = (string)historyListBox.SelectedItem;
			queryTextEditorControl.Text = sql;
			queryTextEditorControl.Refresh();
			tabControl.SelectedTab = queryTabPage;
		}

		private void querySplitContainer_SplitterMoved(object sender, SplitterEventArgs e)
		{
			SQLiteEdit.Properties.Settings.Default.querySplitterDistance = querySplitContainer.SplitterDistance;
		}

		#endregion

		#region Event handlers

		private void MainForm_Load(object sender, EventArgs e)
		{
			versionStatusLabel.Text = "Not Connected";
			ClearTabs();

			foreach (string dbFile in SQLiteEdit.Properties.Settings.Default.dbFiles)
			{
				if (dbFile.Length > 0)
					AddDbToTree(dbFile);
			}
			dbTreeView.TopNode.Expand();
			dbTreeView.Sort();

			RefreshHistory();

			foreach (string file in _fileToLoad)
			{
				OpenDatabase(file);
			}
		}

		private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			SQLiteEdit.Properties.Settings.Default.Save();
		}

		private void MainForm_ResizeEnd(object sender, EventArgs e)
		{
			SQLiteEdit.Properties.Settings.Default.mainFormClientSize = ClientSize;
			SQLiteEdit.Properties.Settings.Default.Save();
		}

		private void openDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (openDbDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
			{
				string file = openDbDialog.FileName;
				OpenDatabase(file);
			}
		}

		private void closeDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
		{
			TreeNode node = dbTreeView.SelectedNode;
			if (node.Level == 1)
			{
				CloseConnection(node.Name);
				UpdateToolBarStates(node);
				SQLiteEdit.Properties.Settings.Default.dbFiles.Remove(node.Name);
				SQLiteEdit.Properties.Settings.Default.Save();
				dbTreeView.Nodes.Remove(node);
			}
		}

		private void connectToolStripButton_Click(object sender, EventArgs e)
		{
			ConnectToSelectedDb();
		}

		private void disconnectToolStripButton_Click(object sender, EventArgs e)
		{
			TreeNode node = dbTreeView.SelectedNode;
			if (node.Level == 1)
			{
				CloseConnection(node.Name);
				UpdateToolBarStates(node);
			}
		}

		private void dbTreeView_AfterSelect(object sender, TreeViewEventArgs e)
		{
			TreeNode node = e.Node;
			UpdateToolBarStates(node);
			DbConnection cnx = GetConnection(node);
			if (cnx != null)
				versionStatusLabel.Text = "SQLite v" + cnx.ServerVersion;
		}

		private void dbTreeView_DoubleClick(object sender, EventArgs e)
		{
			TreeNode node = dbTreeView.SelectedNode;
			if(node == null)
				return;
			if (node.Level == 1)
				ConnectToSelectedDb();
			else if (node.Level == 3)
			{
				DbConnection cnx = _cnxs[node.Parent.Parent.Name];
				bool isTable = node.Parent.Name == "tables";
				bool isView = node.Parent.Name == "views";
				if (isTable || isView)
				{
					if (tabControl.SelectedTab != dataTabPage
							&& (tabControl.SelectedTab != tableTabPage || isView))
					{
						tabControl.SelectedTab = dataTabPage;
					}
					FillDatabaseTab(cnx);
					RefreshDataGrid(cnx, node.Name);
					FillSchema(cnx, node.Name);
					FillQueryTab(cnx);
				}
			}
		}

		private void dbTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if(e.Node.Parent != null)
			{
				bool isTable = e.Node.Parent.Name == "tables";
				bool isView = e.Node.Parent.Name == "views";
				importMenuItem.Visible = false; // isTable;
				importMenuItem.Tag = e.Node;
				exportMenuItem.Visible = isTable;
				exportMenuItem.Tag = e.Node;
			}
		}

		private void aboutSQLiteEditToolStripMenuItem_Click(object sender, EventArgs e)
		{
			AboutBox dialog = new AboutBox();
			dialog.ShowDialog(this);
		}

		private void mainSplitContainer_SplitterMoved(object sender, SplitterEventArgs e)
		{
			SQLiteEdit.Properties.Settings.Default.bottomSplitterDistance = mainSplitContainer.SplitterDistance;
		}

		private void topSplitContainer_SplitterMoved(object sender, SplitterEventArgs e)
		{
			SQLiteEdit.Properties.Settings.Default.topSplitterDistance = topSplitContainer.SplitterDistance;

		}

		private void tablesDataGridView_DoubleClick(object sender, EventArgs e)
		{
			DataGridViewRow row = tablesDataGridView.CurrentRow;
			if(row != null)
			{
				tabControl.SelectedTab = dataTabPage;
				FillDatabaseTab(_currentCnx);
				RefreshDataGrid(_currentCnx, (string)row.Cells[1].Value);
				FillSchema(_currentCnx, (string)row.Cells[1].Value);
				FillQueryTab(_currentCnx);
			}
		}

		private void importMenuItem_Click(object sender, EventArgs e)
		{
			// TODO
		}

		private void exportMenuItem_Click(object sender, EventArgs e)
		{
			TreeNode node = (TreeNode)exportMenuItem.Tag;
			if (node.Level == 3 && node.Parent.Name == "tables")
			{
				DbConnection cnx = _cnxs[node.Parent.Parent.Name];
				string tableName = node.Name;

				SaveFileDialog dialog = new SaveFileDialog();
				dialog.FileName = SQLiteEdit.Properties.Settings.Default.exportFilename;
				dialog.Filter = SQLiteEdit.Properties.Resources.ImportExportFilter;
				if (dialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
				{
					using (StreamWriter writer = new StreamWriter(dialog.FileName, false))
					{
						using (DbCommand cmd = cnx.CreateCommand())
						{
							cmd.CommandText = "SELECT * FROM '" + tableName + "'";
							using (DbDataReader reader = cmd.ExecuteReader())
							{
								{
									List<string> cells = new List<string>();
									for (int i = 0; i < reader.FieldCount; i++)
									{
										cells.Add(reader.GetName(i));
									}
									writer.WriteLine(string.Join(SQLiteEdit.Properties.Settings.Default.csvSeparator, cells.ToArray()));
								}

								while (reader.Read())
								{
									List<string> cells = new List<string>();
									for (int i = 0; i < reader.FieldCount; i++ )
									{
										if(reader.GetFieldType(i) == typeof(string))
										{
											string cell = reader.GetString(i).Replace(@"\", @"\\");
											cell = cell.Replace(Settings.Default.csvStringDelimiter, @"\" + Settings.Default.csvStringDelimiter);
											if (SQLiteEdit.Properties.Settings.Default.csvUseStringDelimiter)
												cells.Add(Settings.Default.csvStringDelimiter + cell + Settings.Default.csvStringDelimiter);
										}
										else
											cells.Add(reader[i].ToString());
									}
									writer.WriteLine(string.Join(SQLiteEdit.Properties.Settings.Default.csvSeparator, cells.ToArray()));
								}
							}
						}
						writer.Flush();
						writer.Close();
					}
				}
			}

		}

		#endregion

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Close();
		}

	}

	public class MyTraceListener : TraceListener
	{
		RichTextBox _output;
		public MyTraceListener(RichTextBox output)
		{
			_output = output;
		}

		public override void Write(string message)
		{
			_output.AppendText(message);
		}

		public override void WriteLine(string message)
		{
			_output.AppendText(message + "\n");
		}
	}


	#region Cache

	/// <summary>
	/// The following code example defines the IDataPageRetriever interface, 
	/// which is implemented by the DataRetriever class. The only method declared 
	/// in this interface is the SupplyPageOfData method, which requires an initial
	/// row index and a count of the number of rows in a single page of data. 
	/// These values are used by the implementer to retrieve a subset of data from 
	/// a data source.
	/// 
	/// A Cache object uses an implementation of this interface during construction 
	/// to load two initial pages of data. Whenever an uncached value is needed, 
	/// the cache discards one of these pages and requests a new page containing 
	/// the value from the IDataPageRetriever
	/// </summary>
	public interface IDataPageRetriever
	{
		DataTable SupplyPageOfData(int lowerPageBoundary, int rowsPerPage);
		int RowCount { get; set; }
		DataColumnCollection Columns { get; }
	}

	/// <summary>
	/// The DataRetriever Class
	/// The following code example defines the DataRetriever class, which implements
	/// the IDataPageRetriever interface to retrieve pages of data from a server. 
	/// The DataRetriever class also provides Columns and RowCount properties, which
	/// the DataGridView control uses to create the necessary columns and to add the
	/// appropriate number of empty rows to the Rows collection. Adding the empty 
	/// rows is necessary so that the control will behave as though it contains all 
	/// the data in the table. This means that the scroll box in the scroll bar will
	/// have the appropriate size, and the user will be able to access any row in 
	/// the table. The rows are filled by the CellValueNeeded event handler only 
	/// when they are scrolled into view. 
	/// </summary>
	public class DataRetriever : IDataPageRetriever
	{
		private readonly string tableName;
		private readonly DbCommand command;
		private readonly string whereClause;
		private readonly string andClause;

        // Declare variables to be reused by the SupplyPageOfData method.
        private string columnToSortBy;
        private readonly DbDataAdapter adapter = new SQLiteDataAdapter();


		public DataRetriever(DbConnection connection, string tableName, string where)
		{
			command = connection.CreateCommand();
			this.tableName = tableName;
			if (where.Length > 0)
			{
				whereClause = " WHERE " + where;
				andClause = " AND (" + where + ")";
			}
			else
			{
				whereClause = "";
				andClause = "";
			}
            adapter.FillError += new FillErrorEventHandler(adapter_FillError);
		}

        void adapter_FillError(object sender, FillErrorEventArgs e)
        {
            //foreach(object obj in e.Values)
            //{
            //    if (obj != null)
            //    {
            //        Console.WriteLine("FillError: Type: {0}   Value: {1}", obj.GetType(), obj);
            //    }
            //    else
            //        Console.WriteLine("FillError: Type: ?   Value: null");
            //}
            // аdd the fixed row to the table
            e.DataTable.Rows.Add(e.Values);
            e.Continue = true;
        }

		private int rowCountValue = -1;

		public int RowCount
		{
			get
			{
				// Return the existing value if it has already been determined.
				if (rowCountValue != -1)
				{
					return rowCountValue;
				}

				// Retrieve the row count from the database.
				command.CommandText = "SELECT COUNT(*) FROM " + tableName + whereClause;
				rowCountValue = (int)(long)command.ExecuteScalar();
				return rowCountValue;
			}
			set
			{
				rowCountValue = value;
			}
		}

		private DataColumnCollection columnsValue;

		public DataColumnCollection Columns
		{
			get
			{
				// Return the existing value if it has already been determined.
				if (columnsValue != null)
				{
					return columnsValue;
				}

				// Retrieve the column information from the database.
				command.CommandText = "SELECT ROWID as ROWID, * FROM " + tableName + whereClause;
				DbDataAdapter localAdapter = new SQLiteDataAdapter();
				localAdapter.SelectCommand = command;
				DataTable table = new DataTable();
				table.Locale = System.Globalization.CultureInfo.InvariantCulture;
				localAdapter.FillSchema(table, SchemaType.Source);
				columnsValue = table.Columns;
				return columnsValue;
			}
		}

		private string commaSeparatedListOfColumnNamesValue = null;

		private string CommaSeparatedListOfColumnNames
		{
			get
			{
				// Return the existing value if it has already been determined.
				if (commaSeparatedListOfColumnNamesValue != null)
				{
					return commaSeparatedListOfColumnNamesValue;
				}

				// Store a list of column names for use in the
				// SupplyPageOfData method.
				System.Text.StringBuilder commaSeparatedColumnNames =
						new System.Text.StringBuilder();
				bool firstColumn = true;
				foreach (DataColumn column in Columns)
				{
					if (!firstColumn)
					{
						commaSeparatedColumnNames.Append(", ");
					}
					commaSeparatedColumnNames.Append(column.ColumnName);
					firstColumn = false;
				}

				commaSeparatedListOfColumnNamesValue =
						commaSeparatedColumnNames.ToString();
				return commaSeparatedListOfColumnNamesValue;
			}
		}

		public DataTable SupplyPageOfData(int lowerPageBoundary, int rowsPerPage)
		{
			// Store the name of the ID column. This column must contain unique 
			// values so the SQL below will work properly.
			if (columnToSortBy == null)
			{
				columnToSortBy = this.Columns[0].ColumnName;
			}

			//if (!this.Columns[columnToSortBy].Unique)
			//{
			//  throw new InvalidOperationException(String.Format(
			//      "Column {0} must contain unique values.", columnToSortBy));
			//}

			// Retrieve the specified number of rows from the database, starting
			// with the row specified by the lowerPageBoundary parameter.
			command.CommandText = "Select " +
					CommaSeparatedListOfColumnNames + " From " + tableName +
					" WHERE " + columnToSortBy + " NOT IN (SELECT " + columnToSortBy +
					" From " + tableName + whereClause + " Order By " + columnToSortBy + " LIMIT " +
					lowerPageBoundary +
					")" + andClause + " Order By " + columnToSortBy + " LIMIT " + rowsPerPage;
			adapter.SelectCommand = command;

			DataTable table = new DataTable();
			table.Locale = System.Globalization.CultureInfo.InvariantCulture;
			adapter.Fill(table);
			return table;
		}
	}

	public class Cache
	{
		private static int RowsPerPage;

		// Represents one page of data.  
		public struct DataPage
		{
			public DataTable table;
			private int lowestIndexValue;
			private int highestIndexValue;

			public DataPage(DataTable table, int rowIndex)
			{
				this.table = table;
				lowestIndexValue = MapToLowerBoundary(rowIndex);
				highestIndexValue = MapToUpperBoundary(rowIndex);
				System.Diagnostics.Debug.Assert(lowestIndexValue >= 0);
				System.Diagnostics.Debug.Assert(highestIndexValue >= 0);
			}

			public int LowestIndex
			{
				get
				{
					return lowestIndexValue;
				}
			}

			public int HighestIndex
			{
				get
				{
					return highestIndexValue;
				}
			}

			public static int MapToLowerBoundary(int rowIndex)
			{
				// Return the lowest index of a page containing the given index.
				return (rowIndex / RowsPerPage) * RowsPerPage;
			}

			private static int MapToUpperBoundary(int rowIndex)
			{
				// Return the highest index of a page containing the given index.
				return MapToLowerBoundary(rowIndex) + RowsPerPage - 1;
			}
		}

		private DataPage[] cachePages;
		private readonly IDataPageRetriever dataSupply;

		public Cache(IDataPageRetriever dataSupplier, int rowsPerPage)
		{
			dataSupply = dataSupplier;
			Cache.RowsPerPage = rowsPerPage;
			LoadFirstTwoPages();
		}

		public int RowCount
		{
			get { return dataSupply.RowCount; }
			set { dataSupply.RowCount = value; }
		}

		public DataColumnCollection Columns
		{
			get { return dataSupply.Columns; }
		}

		// Sets the value of the element parameter if the value is in the cache.
		private bool IfPageCached_ThenSetElement(int rowIndex,
				int columnIndex, ref object element)
		{
			try
			{
				if (IsRowCachedInPage(0, rowIndex))
				{
					element = cachePages[0].table.Rows[rowIndex % RowsPerPage][columnIndex];
					return true;
				}
				else if (IsRowCachedInPage(1, rowIndex))
				{
                    element = cachePages[1].table.Rows[rowIndex % RowsPerPage][columnIndex].ToString();
                    return true;
				}
			}
			catch
			{
				element = "?";
				return true;
			}

			return false;
		}

		public object RetrieveElement(int rowIndex, int columnIndex)
		{
			object element = null;

			if (IfPageCached_ThenSetElement(rowIndex, columnIndex, ref element))
			{
				return element;
			}
			else
			{
				return RetrieveData_CacheIt_ThenReturnElement(
						rowIndex, columnIndex);
			}
		}

		private void LoadFirstTwoPages()
		{
			cachePages = new DataPage[]{
            new DataPage(dataSupply.SupplyPageOfData(
                DataPage.MapToLowerBoundary(0), RowsPerPage), 0), 
            new DataPage(dataSupply.SupplyPageOfData(
                DataPage.MapToLowerBoundary(RowsPerPage), 
                RowsPerPage), RowsPerPage)};
		}

		private object RetrieveData_CacheIt_ThenReturnElement(
				int rowIndex, int columnIndex)
		{
			// Retrieve a page worth of data containing the requested value.
			DataTable table = dataSupply.SupplyPageOfData(
					DataPage.MapToLowerBoundary(rowIndex), RowsPerPage);

			// Replace the cached page furthest from the requested cell
			// with a new page containing the newly retrieved data.
			cachePages[GetIndexToUnusedPage(rowIndex)] = new DataPage(table, rowIndex);

			return RetrieveElement(rowIndex, columnIndex);
		}

		// Returns the index of the cached page most distant from the given index
		// and therefore least likely to be reused.
		private int GetIndexToUnusedPage(int rowIndex)
		{
			if (rowIndex > cachePages[0].HighestIndex &&
					rowIndex > cachePages[1].HighestIndex)
			{
				int offsetFromPage0 = rowIndex - cachePages[0].HighestIndex;
				int offsetFromPage1 = rowIndex - cachePages[1].HighestIndex;
				if (offsetFromPage0 < offsetFromPage1)
				{
					return 1;
				}
				return 0;
			}
			else
			{
				int offsetFromPage0 = cachePages[0].LowestIndex - rowIndex;
				int offsetFromPage1 = cachePages[1].LowestIndex - rowIndex;
				if (offsetFromPage0 < offsetFromPage1)
				{
					return 1;
				}
				return 0;
			}

		}

		// Returns a value indicating whether the given row index is contained
		// in the given DataPage. 
		private bool IsRowCachedInPage(int pageNumber, int rowIndex)
		{
			return rowIndex <= cachePages[pageNumber].HighestIndex &&
					rowIndex >= cachePages[pageNumber].LowestIndex;
		}

	}

	#endregion
}