namespace SQLiteEdit
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("All connections");
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
			this.topSplitContainer = new System.Windows.Forms.SplitContainer();
			this.dbTreeView = new System.Windows.Forms.TreeView();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.importMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.imageList = new System.Windows.Forms.ImageList(this.components);
			this.dbToolStrip = new System.Windows.Forms.ToolStrip();
			this.connectToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.disconnectToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.addTableToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.dropTableToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.addViewToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.dropViewToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.tabControl = new System.Windows.Forms.TabControl();
			this.databaseTabPage = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.settingsDataGridView = new System.Windows.Forms.DataGridView();
			this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label2 = new System.Windows.Forms.Label();
			this.tablesDataGridView = new System.Windows.Forms.DataGridView();
			this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
			this.ColumnTableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnRecordsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnTableQuery = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label1 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.currentDatabaseLabel = new System.Windows.Forms.Label();
			this.tableTabPage = new System.Windows.Forms.TabPage();
			this.schemaSplitContainer = new System.Windows.Forms.SplitContainer();
			this.schemaGridView = new System.Windows.Forms.DataGridView();
			this.FieldIcon = new System.Windows.Forms.DataGridViewImageColumn();
			this.FieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FieldType = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.FieldNotNull = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.FieldUnique = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.FieldDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.createQueryTextBox = new System.Windows.Forms.TextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.currentSchemaLabel = new System.Windows.Forms.Label();
			this.dataTabPage = new System.Windows.Forms.TabPage();
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
			this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
			this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
			this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
			this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.panel1 = new System.Windows.Forms.Panel();
			this.currentTableLabel = new System.Windows.Forms.Label();
			this.queryTabPage = new System.Windows.Forms.TabPage();
			this.querySplitContainer = new System.Windows.Forms.SplitContainer();
			this.queryTextEditorControl = new ICSharpCode.TextEditor.TextEditorControl();
			this.queryGridView = new System.Windows.Forms.DataGridView();
			this.queryToolStrip = new System.Windows.Forms.ToolStrip();
			this.clearQueryToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.executeQueryToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.openQueryToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.saveQueryToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.panel4 = new System.Windows.Forms.Panel();
			this.currentQueryLabel = new System.Windows.Forms.Label();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.timeStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.rowsCountStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.versionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.menuStrip = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.closeDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.queryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.clearQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.executeQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.loadQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutSQLiteEditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
			this.bottomTabControl = new System.Windows.Forms.TabControl();
			this.outputTabPage = new System.Windows.Forms.TabPage();
			this.outputRichTextBox = new System.Windows.Forms.RichTextBox();
			this.historyTabPage = new System.Windows.Forms.TabPage();
			this.historyListBox = new System.Windows.Forms.ListBox();
			this.openDbDialog = new System.Windows.Forms.OpenFileDialog();
			this.openQueryDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveQueryDialog = new System.Windows.Forms.SaveFileDialog();
			this.topSplitContainer.Panel1.SuspendLayout();
			this.topSplitContainer.Panel2.SuspendLayout();
			this.topSplitContainer.SuspendLayout();
			this.contextMenuStrip.SuspendLayout();
			this.dbToolStrip.SuspendLayout();
			this.tabControl.SuspendLayout();
			this.databaseTabPage.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.settingsDataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tablesDataGridView)).BeginInit();
			this.panel3.SuspendLayout();
			this.tableTabPage.SuspendLayout();
			this.schemaSplitContainer.Panel1.SuspendLayout();
			this.schemaSplitContainer.Panel2.SuspendLayout();
			this.schemaSplitContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.schemaGridView)).BeginInit();
			this.panel2.SuspendLayout();
			this.dataTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
			this.bindingNavigator.SuspendLayout();
			this.panel1.SuspendLayout();
			this.queryTabPage.SuspendLayout();
			this.querySplitContainer.Panel1.SuspendLayout();
			this.querySplitContainer.Panel2.SuspendLayout();
			this.querySplitContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.queryGridView)).BeginInit();
			this.queryToolStrip.SuspendLayout();
			this.panel4.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.menuStrip.SuspendLayout();
			this.mainSplitContainer.Panel1.SuspendLayout();
			this.mainSplitContainer.Panel2.SuspendLayout();
			this.mainSplitContainer.SuspendLayout();
			this.bottomTabControl.SuspendLayout();
			this.outputTabPage.SuspendLayout();
			this.historyTabPage.SuspendLayout();
			this.SuspendLayout();
			// 
			// topSplitContainer
			// 
			this.topSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.topSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.topSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.topSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.topSplitContainer.Name = "topSplitContainer";
			// 
			// topSplitContainer.Panel1
			// 
			this.topSplitContainer.Panel1.Controls.Add(this.dbTreeView);
			this.topSplitContainer.Panel1.Controls.Add(this.dbToolStrip);
			// 
			// topSplitContainer.Panel2
			// 
			this.topSplitContainer.Panel2.Controls.Add(this.tabControl);
			this.topSplitContainer.Size = new System.Drawing.Size(696, 350);
			this.topSplitContainer.SplitterDistance = 231;
			this.topSplitContainer.TabIndex = 0;
			this.topSplitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.topSplitContainer_SplitterMoved);
			// 
			// dbTreeView
			// 
			this.dbTreeView.ContextMenuStrip = this.contextMenuStrip;
			this.dbTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dbTreeView.ImageKey = "DatabaseOffline.png";
			this.dbTreeView.ImageList = this.imageList;
			this.dbTreeView.Location = new System.Drawing.Point(0, 25);
			this.dbTreeView.Name = "dbTreeView";
			treeNode1.ImageKey = "SQLiteEdit_16x16.png";
			treeNode1.Name = "RootNode";
			treeNode1.SelectedImageKey = "SQLiteEdit_16x16.png";
			treeNode1.Text = "All connections";
			this.dbTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
			this.dbTreeView.SelectedImageIndex = 0;
			this.dbTreeView.ShowNodeToolTips = true;
			this.dbTreeView.Size = new System.Drawing.Size(227, 321);
			this.dbTreeView.TabIndex = 0;
			this.dbTreeView.DoubleClick += new System.EventHandler(this.dbTreeView_DoubleClick);
			this.dbTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.dbTreeView_AfterSelect);
			this.dbTreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.dbTreeView_NodeMouseClick);
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importMenuItem,
            this.exportMenuItem});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(166, 48);
			// 
			// importMenuItem
			// 
			this.importMenuItem.Name = "importMenuItem";
			this.importMenuItem.Size = new System.Drawing.Size(165, 22);
			this.importMenuItem.Text = "Import table data";
			this.importMenuItem.Click += new System.EventHandler(this.importMenuItem_Click);
			// 
			// exportMenuItem
			// 
			this.exportMenuItem.Name = "exportMenuItem";
			this.exportMenuItem.Size = new System.Drawing.Size(165, 22);
			this.exportMenuItem.Text = "Export table data";
			this.exportMenuItem.Click += new System.EventHandler(this.exportMenuItem_Click);
			// 
			// imageList
			// 
			this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
			this.imageList.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList.Images.SetKeyName(0, "SQLiteEdit_16x16.png");
			this.imageList.Images.SetKeyName(1, "Database.png");
			this.imageList.Images.SetKeyName(2, "DatabaseOffline.png");
			this.imageList.Images.SetKeyName(3, "Table.png");
			this.imageList.Images.SetKeyName(4, "Tables.png");
			this.imageList.Images.SetKeyName(5, "Table_minus.png");
			this.imageList.Images.SetKeyName(6, "Table_plus.png");
			this.imageList.Images.SetKeyName(7, "View.png");
			this.imageList.Images.SetKeyName(8, "Views.png");
			this.imageList.Images.SetKeyName(9, "View_plus.png");
			this.imageList.Images.SetKeyName(10, "ColumnKey.png");
			this.imageList.Images.SetKeyName(11, "ColumnDouble.png");
			this.imageList.Images.SetKeyName(12, "ColumnInt.png");
			this.imageList.Images.SetKeyName(13, "ColumnBlob.png");
			this.imageList.Images.SetKeyName(14, "Columns.png");
			this.imageList.Images.SetKeyName(15, "ColumnText.png");
			this.imageList.Images.SetKeyName(16, "SQL.png");
			this.imageList.Images.SetKeyName(17, "TableSql.png");
			this.imageList.Images.SetKeyName(18, "SystemTable.png");
			// 
			// dbToolStrip
			// 
			this.dbToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectToolStripButton,
            this.disconnectToolStripButton,
            this.toolStripSeparator3,
            this.addTableToolStripButton,
            this.dropTableToolStripButton,
            this.toolStripSeparator4,
            this.addViewToolStripButton,
            this.dropViewToolStripButton});
			this.dbToolStrip.Location = new System.Drawing.Point(0, 0);
			this.dbToolStrip.Name = "dbToolStrip";
			this.dbToolStrip.Size = new System.Drawing.Size(227, 25);
			this.dbToolStrip.TabIndex = 1;
			this.dbToolStrip.Text = "Database toolbar";
			// 
			// connectToolStripButton
			// 
			this.connectToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.connectToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("connectToolStripButton.Image")));
			this.connectToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.connectToolStripButton.Name = "connectToolStripButton";
			this.connectToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.connectToolStripButton.Text = "Connect";
			this.connectToolStripButton.Click += new System.EventHandler(this.connectToolStripButton_Click);
			// 
			// disconnectToolStripButton
			// 
			this.disconnectToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.disconnectToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("disconnectToolStripButton.Image")));
			this.disconnectToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.disconnectToolStripButton.Name = "disconnectToolStripButton";
			this.disconnectToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.disconnectToolStripButton.Text = "Disconnect";
			this.disconnectToolStripButton.Click += new System.EventHandler(this.disconnectToolStripButton_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// addTableToolStripButton
			// 
			this.addTableToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.addTableToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addTableToolStripButton.Image")));
			this.addTableToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.addTableToolStripButton.Name = "addTableToolStripButton";
			this.addTableToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.addTableToolStripButton.Text = "Create table";
			// 
			// dropTableToolStripButton
			// 
			this.dropTableToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.dropTableToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("dropTableToolStripButton.Image")));
			this.dropTableToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.dropTableToolStripButton.Name = "dropTableToolStripButton";
			this.dropTableToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.dropTableToolStripButton.Text = "Drop table";
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// addViewToolStripButton
			// 
			this.addViewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.addViewToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addViewToolStripButton.Image")));
			this.addViewToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.addViewToolStripButton.Name = "addViewToolStripButton";
			this.addViewToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.addViewToolStripButton.Text = "Create View";
			// 
			// dropViewToolStripButton
			// 
			this.dropViewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.dropViewToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("dropViewToolStripButton.Image")));
			this.dropViewToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.dropViewToolStripButton.Name = "dropViewToolStripButton";
			this.dropViewToolStripButton.Size = new System.Drawing.Size(23, 22);
			this.dropViewToolStripButton.Text = "Drop View";
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.databaseTabPage);
			this.tabControl.Controls.Add(this.tableTabPage);
			this.tabControl.Controls.Add(this.dataTabPage);
			this.tabControl.Controls.Add(this.queryTabPage);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.ImageList = this.imageList;
			this.tabControl.Location = new System.Drawing.Point(0, 0);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(457, 346);
			this.tabControl.TabIndex = 0;
			// 
			// databaseTabPage
			// 
			this.databaseTabPage.Controls.Add(this.tableLayoutPanel1);
			this.databaseTabPage.Controls.Add(this.panel3);
			this.databaseTabPage.ImageKey = "Database.png";
			this.databaseTabPage.Location = new System.Drawing.Point(4, 23);
			this.databaseTabPage.Name = "databaseTabPage";
			this.databaseTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.databaseTabPage.Size = new System.Drawing.Size(449, 319);
			this.databaseTabPage.TabIndex = 0;
			this.databaseTabPage.Text = "Database";
			this.databaseTabPage.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.997743F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93.00226F));
			this.tableLayoutPanel1.Controls.Add(this.settingsDataGridView, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.tablesDataGridView, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 25);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(443, 291);
			this.tableLayoutPanel1.TabIndex = 5;
			// 
			// settingsDataGridView
			// 
			this.settingsDataGridView.AllowUserToAddRows = false;
			this.settingsDataGridView.AllowUserToDeleteRows = false;
			this.settingsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.settingsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.settingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.settingsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewImageColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.settingsDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
			this.settingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.settingsDataGridView.Location = new System.Drawing.Point(34, 170);
			this.settingsDataGridView.Name = "settingsDataGridView";
			this.settingsDataGridView.ReadOnly = true;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.settingsDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.settingsDataGridView.RowTemplate.Height = 18;
			this.settingsDataGridView.Size = new System.Drawing.Size(406, 118);
			this.settingsDataGridView.TabIndex = 8;
			// 
			// dataGridViewImageColumn2
			// 
			this.dataGridViewImageColumn2.HeaderText = "";
			this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
			this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
			this.dataGridViewImageColumn2.ReadOnly = true;
			this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewImageColumn2.Width = 5;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.HeaderText = "Name";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Width = 60;
			// 
			// dataGridViewTextBoxColumn4
			// 
			this.dataGridViewTextBoxColumn4.HeaderText = "Value";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.ReadOnly = true;
			this.dataGridViewTextBoxColumn4.Width = 59;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.label2, 2);
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(3, 145);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(437, 22);
			this.label2.TabIndex = 7;
			this.label2.Text = "Database settings";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tablesDataGridView
			// 
			this.tablesDataGridView.AllowUserToAddRows = false;
			this.tablesDataGridView.AllowUserToDeleteRows = false;
			this.tablesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.tablesDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this.tablesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.tablesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewImageColumn1,
            this.ColumnTableName,
            this.ColumnRecordsCount,
            this.ColumnTableQuery});
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.tablesDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
			this.tablesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tablesDataGridView.Location = new System.Drawing.Point(34, 25);
			this.tablesDataGridView.Name = "tablesDataGridView";
			this.tablesDataGridView.ReadOnly = true;
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.tablesDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
			this.tablesDataGridView.RowTemplate.Height = 18;
			this.tablesDataGridView.Size = new System.Drawing.Size(406, 117);
			this.tablesDataGridView.TabIndex = 6;
			this.tablesDataGridView.DoubleClick += new System.EventHandler(this.tablesDataGridView_DoubleClick);
			// 
			// dataGridViewImageColumn1
			// 
			this.dataGridViewImageColumn1.HeaderText = "";
			this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
			this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
			this.dataGridViewImageColumn1.ReadOnly = true;
			this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewImageColumn1.Width = 5;
			// 
			// ColumnTableName
			// 
			this.ColumnTableName.HeaderText = "Table";
			this.ColumnTableName.Name = "ColumnTableName";
			this.ColumnTableName.ReadOnly = true;
			this.ColumnTableName.Width = 59;
			// 
			// ColumnRecordsCount
			// 
			this.ColumnRecordsCount.HeaderText = "Records";
			this.ColumnRecordsCount.Name = "ColumnRecordsCount";
			this.ColumnRecordsCount.ReadOnly = true;
			this.ColumnRecordsCount.Width = 72;
			// 
			// ColumnTableQuery
			// 
			this.ColumnTableQuery.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnTableQuery.HeaderText = "Query";
			this.ColumnTableQuery.Name = "ColumnTableQuery";
			this.ColumnTableQuery.ReadOnly = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(437, 22);
			this.label1.TabIndex = 0;
			this.label1.Text = "Tables / Views";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.panel3.Controls.Add(this.currentDatabaseLabel);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(3, 3);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(443, 22);
			this.panel3.TabIndex = 4;
			// 
			// currentDatabaseLabel
			// 
			this.currentDatabaseLabel.AutoSize = true;
			this.currentDatabaseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.currentDatabaseLabel.Location = new System.Drawing.Point(4, 4);
			this.currentDatabaseLabel.Name = "currentDatabaseLabel";
			this.currentDatabaseLabel.Size = new System.Drawing.Size(139, 13);
			this.currentDatabaseLabel.TabIndex = 0;
			this.currentDatabaseLabel.Text = "Database : Table name";
			// 
			// tableTabPage
			// 
			this.tableTabPage.Controls.Add(this.schemaSplitContainer);
			this.tableTabPage.Controls.Add(this.panel2);
			this.tableTabPage.ImageKey = "Columns.png";
			this.tableTabPage.Location = new System.Drawing.Point(4, 23);
			this.tableTabPage.Name = "tableTabPage";
			this.tableTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.tableTabPage.Size = new System.Drawing.Size(449, 319);
			this.tableTabPage.TabIndex = 1;
			this.tableTabPage.Text = "Table";
			this.tableTabPage.UseVisualStyleBackColor = true;
			// 
			// schemaSplitContainer
			// 
			this.schemaSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.schemaSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.schemaSplitContainer.Location = new System.Drawing.Point(3, 25);
			this.schemaSplitContainer.Name = "schemaSplitContainer";
			this.schemaSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// schemaSplitContainer.Panel1
			// 
			this.schemaSplitContainer.Panel1.Controls.Add(this.schemaGridView);
			// 
			// schemaSplitContainer.Panel2
			// 
			this.schemaSplitContainer.Panel2.Controls.Add(this.createQueryTextBox);
			this.schemaSplitContainer.Size = new System.Drawing.Size(443, 291);
			this.schemaSplitContainer.SplitterDistance = 223;
			this.schemaSplitContainer.TabIndex = 6;
			this.schemaSplitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.schemaSplitContainer_SplitterMoved);
			// 
			// schemaGridView
			// 
			this.schemaGridView.AllowUserToAddRows = false;
			this.schemaGridView.AllowUserToDeleteRows = false;
			this.schemaGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
			dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.schemaGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.schemaGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.schemaGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FieldIcon,
            this.FieldName,
            this.FieldType,
            this.FieldNotNull,
            this.FieldUnique,
            this.FieldDefault});
			dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.schemaGridView.DefaultCellStyle = dataGridViewCellStyle8;
			this.schemaGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.schemaGridView.Location = new System.Drawing.Point(0, 0);
			this.schemaGridView.Name = "schemaGridView";
			this.schemaGridView.ReadOnly = true;
			dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.schemaGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
			this.schemaGridView.RowTemplate.Height = 18;
			this.schemaGridView.Size = new System.Drawing.Size(443, 223);
			this.schemaGridView.TabIndex = 5;
			this.schemaGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.schemaGridView_DataError);
			// 
			// FieldIcon
			// 
			this.FieldIcon.HeaderText = "";
			this.FieldIcon.Name = "FieldIcon";
			this.FieldIcon.ReadOnly = true;
			this.FieldIcon.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.FieldIcon.Width = 5;
			// 
			// FieldName
			// 
			this.FieldName.HeaderText = "Name";
			this.FieldName.Name = "FieldName";
			this.FieldName.ReadOnly = true;
			this.FieldName.Width = 60;
			// 
			// FieldType
			// 
			this.FieldType.HeaderText = "Data type";
			this.FieldType.Items.AddRange(new object[] {
            "INTEGER",
            "DOUBLE",
            "TEXT",
            "BLOB",
            "DATETIME"});
			this.FieldType.Name = "FieldType";
			this.FieldType.ReadOnly = true;
			this.FieldType.Width = 59;
			// 
			// FieldNotNull
			// 
			this.FieldNotNull.HeaderText = "Not NULL";
			this.FieldNotNull.Name = "FieldNotNull";
			this.FieldNotNull.ReadOnly = true;
			this.FieldNotNull.Width = 61;
			// 
			// FieldUnique
			// 
			this.FieldUnique.HeaderText = "Unique";
			this.FieldUnique.Name = "FieldUnique";
			this.FieldUnique.ReadOnly = true;
			this.FieldUnique.Width = 47;
			// 
			// FieldDefault
			// 
			this.FieldDefault.HeaderText = "Default value";
			this.FieldDefault.Name = "FieldDefault";
			this.FieldDefault.ReadOnly = true;
			this.FieldDefault.Width = 95;
			// 
			// createQueryTextBox
			// 
			this.createQueryTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.createQueryTextBox.Location = new System.Drawing.Point(0, 0);
			this.createQueryTextBox.Multiline = true;
			this.createQueryTextBox.Name = "createQueryTextBox";
			this.createQueryTextBox.ReadOnly = true;
			this.createQueryTextBox.Size = new System.Drawing.Size(443, 64);
			this.createQueryTextBox.TabIndex = 0;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.panel2.Controls.Add(this.currentSchemaLabel);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(3, 3);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(443, 22);
			this.panel2.TabIndex = 3;
			// 
			// currentSchemaLabel
			// 
			this.currentSchemaLabel.AutoSize = true;
			this.currentSchemaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.currentSchemaLabel.Location = new System.Drawing.Point(4, 4);
			this.currentSchemaLabel.Name = "currentSchemaLabel";
			this.currentSchemaLabel.Size = new System.Drawing.Size(139, 13);
			this.currentSchemaLabel.TabIndex = 0;
			this.currentSchemaLabel.Text = "Database : Table name";
			// 
			// dataTabPage
			// 
			this.dataTabPage.Controls.Add(this.dataGridView);
			this.dataTabPage.Controls.Add(this.bindingNavigator);
			this.dataTabPage.Controls.Add(this.panel1);
			this.dataTabPage.ImageKey = "Table.png";
			this.dataTabPage.Location = new System.Drawing.Point(4, 23);
			this.dataTabPage.Name = "dataTabPage";
			this.dataTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.dataTabPage.Size = new System.Drawing.Size(449, 319);
			this.dataTabPage.TabIndex = 2;
			this.dataTabPage.Text = "Data";
			this.dataTabPage.UseVisualStyleBackColor = true;
			// 
			// dataGridView
			// 
			this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
			dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridView.DefaultCellStyle = dataGridViewCellStyle11;
			this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView.Location = new System.Drawing.Point(3, 25);
			this.dataGridView.Name = "dataGridView";
			dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
			this.dataGridView.RowTemplate.Height = 18;
			this.dataGridView.Size = new System.Drawing.Size(443, 266);
			this.dataGridView.TabIndex = 4;
			this.dataGridView.VirtualMode = true;
			this.dataGridView.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView_UserDeletingRow);
			this.dataGridView.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridView_UserAddedRow);
			this.dataGridView.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView_RowValidating);
			this.dataGridView.CancelRowEdit += new System.Windows.Forms.QuestionEventHandler(this.dataGridView_CancelRowEdit);
			this.dataGridView.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dataGridView_CellValueNeeded);
			this.dataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
			this.dataGridView.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_RowLeave);
			this.dataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView_RowsAdded);
			this.dataGridView.CellValuePushed += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dataGridView_CellValuePushed);
			this.dataGridView.NewRowNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridView_NewRowNeeded);
			// 
			// bindingNavigator
			// 
			this.bindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
			this.bindingNavigator.CountItem = this.bindingNavigatorCountItem;
			this.bindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
			this.bindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
			this.bindingNavigator.Location = new System.Drawing.Point(3, 291);
			this.bindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
			this.bindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
			this.bindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
			this.bindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
			this.bindingNavigator.Name = "bindingNavigator";
			this.bindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
			this.bindingNavigator.Size = new System.Drawing.Size(443, 25);
			this.bindingNavigator.TabIndex = 3;
			this.bindingNavigator.Text = "bindingNavigator1";
			// 
			// bindingNavigatorAddNewItem
			// 
			this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
			this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
			this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorAddNewItem.Text = "Add new";
			// 
			// bindingNavigatorCountItem
			// 
			this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
			this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
			this.bindingNavigatorCountItem.Text = "of {0}";
			this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
			// 
			// bindingNavigatorDeleteItem
			// 
			this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
			this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
			this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorDeleteItem.Text = "Delete";
			// 
			// bindingNavigatorMoveFirstItem
			// 
			this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
			this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
			this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveFirstItem.Text = "Move first";
			// 
			// bindingNavigatorMovePreviousItem
			// 
			this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
			this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
			this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMovePreviousItem.Text = "Move previous";
			// 
			// bindingNavigatorSeparator
			// 
			this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
			this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorPositionItem
			// 
			this.bindingNavigatorPositionItem.AccessibleName = "Position";
			this.bindingNavigatorPositionItem.AutoSize = false;
			this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
			this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
			this.bindingNavigatorPositionItem.Text = "0";
			this.bindingNavigatorPositionItem.ToolTipText = "Current position";
			// 
			// bindingNavigatorSeparator1
			// 
			this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
			this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorMoveNextItem
			// 
			this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
			this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
			this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveNextItem.Text = "Move next";
			// 
			// bindingNavigatorMoveLastItem
			// 
			this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
			this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
			this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveLastItem.Text = "Move last";
			// 
			// bindingNavigatorSeparator2
			// 
			this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
			this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.panel1.Controls.Add(this.currentTableLabel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(3, 3);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(443, 22);
			this.panel1.TabIndex = 2;
			// 
			// currentTableLabel
			// 
			this.currentTableLabel.AutoSize = true;
			this.currentTableLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.currentTableLabel.Location = new System.Drawing.Point(4, 4);
			this.currentTableLabel.Name = "currentTableLabel";
			this.currentTableLabel.Size = new System.Drawing.Size(139, 13);
			this.currentTableLabel.TabIndex = 0;
			this.currentTableLabel.Text = "Database : Table name";
			// 
			// queryTabPage
			// 
			this.queryTabPage.Controls.Add(this.querySplitContainer);
			this.queryTabPage.Controls.Add(this.queryToolStrip);
			this.queryTabPage.Controls.Add(this.panel4);
			this.queryTabPage.ImageKey = "SQL.png";
			this.queryTabPage.Location = new System.Drawing.Point(4, 23);
			this.queryTabPage.Name = "queryTabPage";
			this.queryTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.queryTabPage.Size = new System.Drawing.Size(449, 319);
			this.queryTabPage.TabIndex = 3;
			this.queryTabPage.Text = "Query";
			this.queryTabPage.UseVisualStyleBackColor = true;
			// 
			// querySplitContainer
			// 
			this.querySplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.querySplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.querySplitContainer.Location = new System.Drawing.Point(27, 25);
			this.querySplitContainer.Name = "querySplitContainer";
			this.querySplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// querySplitContainer.Panel1
			// 
			this.querySplitContainer.Panel1.Controls.Add(this.queryTextEditorControl);
			// 
			// querySplitContainer.Panel2
			// 
			this.querySplitContainer.Panel2.Controls.Add(this.queryGridView);
			this.querySplitContainer.Size = new System.Drawing.Size(419, 291);
			this.querySplitContainer.SplitterDistance = 110;
			this.querySplitContainer.TabIndex = 7;
			this.querySplitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.querySplitContainer_SplitterMoved);
			// 
			// queryTextEditorControl
			// 
			this.queryTextEditorControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.queryTextEditorControl.Location = new System.Drawing.Point(0, 0);
			this.queryTextEditorControl.Name = "queryTextEditorControl";
			this.queryTextEditorControl.ShowEOLMarkers = true;
			this.queryTextEditorControl.ShowInvalidLines = false;
			this.queryTextEditorControl.ShowSpaces = true;
			this.queryTextEditorControl.ShowTabs = true;
			this.queryTextEditorControl.ShowVRuler = true;
			this.queryTextEditorControl.Size = new System.Drawing.Size(419, 110);
			this.queryTextEditorControl.TabIndex = 6;
			// 
			// queryGridView
			// 
			this.queryGridView.AllowUserToAddRows = false;
			this.queryGridView.AllowUserToDeleteRows = false;
			this.queryGridView.AllowUserToOrderColumns = true;
			this.queryGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
			dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.queryGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
			this.queryGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.queryGridView.DefaultCellStyle = dataGridViewCellStyle14;
			this.queryGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.queryGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.queryGridView.Location = new System.Drawing.Point(0, 0);
			this.queryGridView.MultiSelect = false;
			this.queryGridView.Name = "queryGridView";
			this.queryGridView.ReadOnly = true;
			dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.queryGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
			this.queryGridView.RowTemplate.Height = 18;
			this.queryGridView.Size = new System.Drawing.Size(419, 177);
			this.queryGridView.TabIndex = 7;
			this.queryGridView.TabStop = false;
			// 
			// queryToolStrip
			// 
			this.queryToolStrip.Dock = System.Windows.Forms.DockStyle.Left;
			this.queryToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearQueryToolStripButton,
            this.executeQueryToolStripButton,
            this.toolStripSeparator5,
            this.openQueryToolStripButton,
            this.saveQueryToolStripButton});
			this.queryToolStrip.Location = new System.Drawing.Point(3, 25);
			this.queryToolStrip.Name = "queryToolStrip";
			this.queryToolStrip.Size = new System.Drawing.Size(24, 291);
			this.queryToolStrip.TabIndex = 5;
			this.queryToolStrip.Text = "toolStrip1";
			this.queryToolStrip.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
			// 
			// clearQueryToolStripButton
			// 
			this.clearQueryToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.clearQueryToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("clearQueryToolStripButton.Image")));
			this.clearQueryToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.clearQueryToolStripButton.Name = "clearQueryToolStripButton";
			this.clearQueryToolStripButton.Size = new System.Drawing.Size(21, 20);
			this.clearQueryToolStripButton.Text = "Clear query";
			this.clearQueryToolStripButton.Click += new System.EventHandler(this.clearQueryToolStripButton_Click);
			// 
			// executeQueryToolStripButton
			// 
			this.executeQueryToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.executeQueryToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("executeQueryToolStripButton.Image")));
			this.executeQueryToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.executeQueryToolStripButton.Name = "executeQueryToolStripButton";
			this.executeQueryToolStripButton.Size = new System.Drawing.Size(21, 20);
			this.executeQueryToolStripButton.Text = "Execute query (F9)";
			this.executeQueryToolStripButton.Click += new System.EventHandler(this.executeQueryToolStripButton_Click);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(21, 6);
			this.toolStripSeparator5.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90;
			// 
			// openQueryToolStripButton
			// 
			this.openQueryToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.openQueryToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openQueryToolStripButton.Image")));
			this.openQueryToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.openQueryToolStripButton.Name = "openQueryToolStripButton";
			this.openQueryToolStripButton.Size = new System.Drawing.Size(21, 20);
			this.openQueryToolStripButton.Text = "Load a query (Ctrl+L)";
			this.openQueryToolStripButton.Click += new System.EventHandler(this.loadQueryToolStripMenuItem_Click);
			// 
			// saveQueryToolStripButton
			// 
			this.saveQueryToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.saveQueryToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveQueryToolStripButton.Image")));
			this.saveQueryToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.saveQueryToolStripButton.Name = "saveQueryToolStripButton";
			this.saveQueryToolStripButton.Size = new System.Drawing.Size(21, 20);
			this.saveQueryToolStripButton.Text = "Save query (Ctrl+S)";
			this.saveQueryToolStripButton.Click += new System.EventHandler(this.saveQueryToolStripMenuItem_Click);
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.panel4.Controls.Add(this.currentQueryLabel);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel4.Location = new System.Drawing.Point(3, 3);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(443, 22);
			this.panel4.TabIndex = 4;
			// 
			// currentQueryLabel
			// 
			this.currentQueryLabel.AutoSize = true;
			this.currentQueryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.currentQueryLabel.Location = new System.Drawing.Point(4, 4);
			this.currentQueryLabel.Name = "currentQueryLabel";
			this.currentQueryLabel.Size = new System.Drawing.Size(139, 13);
			this.currentQueryLabel.TabIndex = 0;
			this.currentQueryLabel.Text = "Database : Table name";
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.timeStatusLabel,
            this.rowsCountStatusLabel,
            this.versionStatusLabel,
            this.statusLabel});
			this.statusStrip.Location = new System.Drawing.Point(0, 473);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(696, 24);
			this.statusStrip.TabIndex = 1;
			this.statusStrip.Text = "statusStrip1";
			// 
			// timeStatusLabel
			// 
			this.timeStatusLabel.AutoSize = false;
			this.timeStatusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.timeStatusLabel.Name = "timeStatusLabel";
			this.timeStatusLabel.Size = new System.Drawing.Size(95, 19);
			this.timeStatusLabel.Text = "Time: 000,0 ms";
			this.timeStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// rowsCountStatusLabel
			// 
			this.rowsCountStatusLabel.AutoSize = false;
			this.rowsCountStatusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.rowsCountStatusLabel.Name = "rowsCountStatusLabel";
			this.rowsCountStatusLabel.Size = new System.Drawing.Size(90, 19);
			this.rowsCountStatusLabel.Text = "000 000 rows";
			this.rowsCountStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// versionStatusLabel
			// 
			this.versionStatusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.versionStatusLabel.Name = "versionStatusLabel";
			this.versionStatusLabel.Size = new System.Drawing.Size(16, 19);
			this.versionStatusLabel.Text = "-";
			// 
			// statusLabel
			// 
			this.statusLabel.Name = "statusLabel";
			this.statusLabel.Size = new System.Drawing.Size(480, 19);
			this.statusLabel.Spring = true;
			// 
			// menuStrip
			// 
			this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.queryToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip.Location = new System.Drawing.Point(0, 0);
			this.menuStrip.Name = "menuStrip";
			this.menuStrip.Size = new System.Drawing.Size(696, 24);
			this.menuStrip.TabIndex = 1;
			this.menuStrip.Text = "menuStrip";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openDatabaseToolStripMenuItem,
            this.closeDatabaseToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// openDatabaseToolStripMenuItem
			// 
			this.openDatabaseToolStripMenuItem.Name = "openDatabaseToolStripMenuItem";
			this.openDatabaseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openDatabaseToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
			this.openDatabaseToolStripMenuItem.Text = "Open database";
			this.openDatabaseToolStripMenuItem.Click += new System.EventHandler(this.openDatabaseToolStripMenuItem_Click);
			// 
			// closeDatabaseToolStripMenuItem
			// 
			this.closeDatabaseToolStripMenuItem.Name = "closeDatabaseToolStripMenuItem";
			this.closeDatabaseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
			this.closeDatabaseToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
			this.closeDatabaseToolStripMenuItem.Text = "Close database";
			this.closeDatabaseToolStripMenuItem.Click += new System.EventHandler(this.closeDatabaseToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// queryToolStripMenuItem
			// 
			this.queryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearQueryToolStripMenuItem,
            this.executeQueryToolStripMenuItem,
            this.toolStripSeparator2,
            this.loadQueryToolStripMenuItem,
            this.saveQueryToolStripMenuItem});
			this.queryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("queryToolStripMenuItem.Image")));
			this.queryToolStripMenuItem.Name = "queryToolStripMenuItem";
			this.queryToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
			this.queryToolStripMenuItem.Text = "Query";
			// 
			// clearQueryToolStripMenuItem
			// 
			this.clearQueryToolStripMenuItem.Name = "clearQueryToolStripMenuItem";
			this.clearQueryToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
			this.clearQueryToolStripMenuItem.Text = "Clear query";
			// 
			// executeQueryToolStripMenuItem
			// 
			this.executeQueryToolStripMenuItem.Name = "executeQueryToolStripMenuItem";
			this.executeQueryToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9;
			this.executeQueryToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
			this.executeQueryToolStripMenuItem.Text = "Execute query";
			this.executeQueryToolStripMenuItem.Click += new System.EventHandler(this.executeQueryToolStripButton_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(179, 6);
			// 
			// loadQueryToolStripMenuItem
			// 
			this.loadQueryToolStripMenuItem.Name = "loadQueryToolStripMenuItem";
			this.loadQueryToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
			this.loadQueryToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
			this.loadQueryToolStripMenuItem.Text = "Load a query";
			this.loadQueryToolStripMenuItem.Click += new System.EventHandler(this.loadQueryToolStripMenuItem_Click);
			// 
			// saveQueryToolStripMenuItem
			// 
			this.saveQueryToolStripMenuItem.Name = "saveQueryToolStripMenuItem";
			this.saveQueryToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveQueryToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
			this.saveQueryToolStripMenuItem.Text = "Save query";
			this.saveQueryToolStripMenuItem.Click += new System.EventHandler(this.saveQueryToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutSQLiteEditToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// aboutSQLiteEditToolStripMenuItem
			// 
			this.aboutSQLiteEditToolStripMenuItem.Name = "aboutSQLiteEditToolStripMenuItem";
			this.aboutSQLiteEditToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.aboutSQLiteEditToolStripMenuItem.Text = "About SQLiteEdit";
			this.aboutSQLiteEditToolStripMenuItem.Click += new System.EventHandler(this.aboutSQLiteEditToolStripMenuItem_Click);
			// 
			// mainSplitContainer
			// 
			this.mainSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.mainSplitContainer.Location = new System.Drawing.Point(0, 24);
			this.mainSplitContainer.Name = "mainSplitContainer";
			this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// mainSplitContainer.Panel1
			// 
			this.mainSplitContainer.Panel1.Controls.Add(this.topSplitContainer);
			// 
			// mainSplitContainer.Panel2
			// 
			this.mainSplitContainer.Panel2.Controls.Add(this.bottomTabControl);
			this.mainSplitContainer.Size = new System.Drawing.Size(696, 449);
			this.mainSplitContainer.SplitterDistance = 350;
			this.mainSplitContainer.TabIndex = 2;
			this.mainSplitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.mainSplitContainer_SplitterMoved);
			// 
			// bottomTabControl
			// 
			this.bottomTabControl.Controls.Add(this.outputTabPage);
			this.bottomTabControl.Controls.Add(this.historyTabPage);
			this.bottomTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.bottomTabControl.Location = new System.Drawing.Point(0, 0);
			this.bottomTabControl.Name = "bottomTabControl";
			this.bottomTabControl.SelectedIndex = 0;
			this.bottomTabControl.Size = new System.Drawing.Size(692, 91);
			this.bottomTabControl.TabIndex = 0;
			// 
			// outputTabPage
			// 
			this.outputTabPage.Controls.Add(this.outputRichTextBox);
			this.outputTabPage.Location = new System.Drawing.Point(4, 22);
			this.outputTabPage.Name = "outputTabPage";
			this.outputTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.outputTabPage.Size = new System.Drawing.Size(684, 65);
			this.outputTabPage.TabIndex = 0;
			this.outputTabPage.Text = "Output";
			this.outputTabPage.UseVisualStyleBackColor = true;
			// 
			// outputRichTextBox
			// 
			this.outputRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.outputRichTextBox.Location = new System.Drawing.Point(3, 3);
			this.outputRichTextBox.Name = "outputRichTextBox";
			this.outputRichTextBox.Size = new System.Drawing.Size(678, 59);
			this.outputRichTextBox.TabIndex = 0;
			this.outputRichTextBox.Text = "";
			// 
			// historyTabPage
			// 
			this.historyTabPage.Controls.Add(this.historyListBox);
			this.historyTabPage.Location = new System.Drawing.Point(4, 22);
			this.historyTabPage.Name = "historyTabPage";
			this.historyTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.historyTabPage.Size = new System.Drawing.Size(684, 65);
			this.historyTabPage.TabIndex = 1;
			this.historyTabPage.Text = "History";
			this.historyTabPage.UseVisualStyleBackColor = true;
			// 
			// historyListBox
			// 
			this.historyListBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.historyListBox.FormattingEnabled = true;
			this.historyListBox.Location = new System.Drawing.Point(3, 3);
			this.historyListBox.Name = "historyListBox";
			this.historyListBox.Size = new System.Drawing.Size(678, 56);
			this.historyListBox.TabIndex = 0;
			this.historyListBox.DoubleClick += new System.EventHandler(this.historyListBox_DoubleClick);
			// 
			// openDbDialog
			// 
			this.openDbDialog.FileName = "sqlite.db3";
			this.openDbDialog.Filter = "SQLite3 database|*.db;*.db3|All files|*.*";
			// 
			// openQueryDialog
			// 
			this.openQueryDialog.FileName = global::SQLiteEdit.Properties.Settings.Default.queryFilename;
			this.openQueryDialog.Filter = "Query files|*.sql|Text files|*.txt|All files|*.*";
			this.openQueryDialog.Title = "Loading a query";
			// 
			// saveQueryDialog
			// 
			this.saveQueryDialog.DefaultExt = "sql";
			this.saveQueryDialog.FileName = global::SQLiteEdit.Properties.Settings.Default.queryFilename;
			this.saveQueryDialog.Filter = "Query files|*.sql|Text files|*.txt|All files|*.*";
			this.saveQueryDialog.Title = "Saving a query";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(696, 497);
			this.Controls.Add(this.mainSplitContainer);
			this.Controls.Add(this.statusStrip);
			this.Controls.Add(this.menuStrip);
			this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::SQLiteEdit.Properties.Settings.Default, "mainFormLocation", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = global::SQLiteEdit.Properties.Settings.Default.mainFormLocation;
			this.MainMenuStrip = this.menuStrip;
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
			this.topSplitContainer.Panel1.ResumeLayout(false);
			this.topSplitContainer.Panel1.PerformLayout();
			this.topSplitContainer.Panel2.ResumeLayout(false);
			this.topSplitContainer.ResumeLayout(false);
			this.contextMenuStrip.ResumeLayout(false);
			this.dbToolStrip.ResumeLayout(false);
			this.dbToolStrip.PerformLayout();
			this.tabControl.ResumeLayout(false);
			this.databaseTabPage.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.settingsDataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tablesDataGridView)).EndInit();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.tableTabPage.ResumeLayout(false);
			this.schemaSplitContainer.Panel1.ResumeLayout(false);
			this.schemaSplitContainer.Panel2.ResumeLayout(false);
			this.schemaSplitContainer.Panel2.PerformLayout();
			this.schemaSplitContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.schemaGridView)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.dataTabPage.ResumeLayout(false);
			this.dataTabPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
			this.bindingNavigator.ResumeLayout(false);
			this.bindingNavigator.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.queryTabPage.ResumeLayout(false);
			this.queryTabPage.PerformLayout();
			this.querySplitContainer.Panel1.ResumeLayout(false);
			this.querySplitContainer.Panel2.ResumeLayout(false);
			this.querySplitContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.queryGridView)).EndInit();
			this.queryToolStrip.ResumeLayout(false);
			this.queryToolStrip.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.menuStrip.ResumeLayout(false);
			this.menuStrip.PerformLayout();
			this.mainSplitContainer.Panel1.ResumeLayout(false);
			this.mainSplitContainer.Panel2.ResumeLayout(false);
			this.mainSplitContainer.ResumeLayout(false);
			this.bottomTabControl.ResumeLayout(false);
			this.outputTabPage.ResumeLayout(false);
			this.historyTabPage.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.SplitContainer topSplitContainer;
		private System.Windows.Forms.TreeView dbTreeView;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.MenuStrip menuStrip;
		private System.Windows.Forms.SplitContainer mainSplitContainer;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage databaseTabPage;
		private System.Windows.Forms.TabPage tableTabPage;
		private System.Windows.Forms.TabControl bottomTabControl;
		private System.Windows.Forms.TabPage outputTabPage;
		private System.Windows.Forms.TabPage historyTabPage;
		private System.Windows.Forms.TabPage dataTabPage;
		private System.Windows.Forms.TabPage queryTabPage;
		private System.Windows.Forms.ImageList imageList;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openDatabaseToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem closeDatabaseToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem queryToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem clearQueryToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem executeQueryToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem loadQueryToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveQueryToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutSQLiteEditToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog openDbDialog;
		private System.Windows.Forms.ToolStrip dbToolStrip;
		private System.Windows.Forms.ToolStripButton connectToolStripButton;
		private System.Windows.Forms.ToolStripButton disconnectToolStripButton;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton addTableToolStripButton;
		private System.Windows.Forms.ToolStripButton dropTableToolStripButton;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripButton addViewToolStripButton;
		private System.Windows.Forms.ToolStripButton dropViewToolStripButton;
		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.BindingNavigator bindingNavigator;
		private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
		private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
		private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
		private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
		private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label currentTableLabel;
		private System.Windows.Forms.ToolStripStatusLabel timeStatusLabel;
		private System.Windows.Forms.ToolStripStatusLabel rowsCountStatusLabel;
		private System.Windows.Forms.ToolStripStatusLabel versionStatusLabel;
		private System.Windows.Forms.ToolStripStatusLabel statusLabel;
		private System.Windows.Forms.DataGridView schemaGridView;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label currentSchemaLabel;
		private System.Windows.Forms.RichTextBox outputRichTextBox;
		private System.Windows.Forms.ListBox historyListBox;
		private System.Windows.Forms.DataGridViewImageColumn FieldIcon;
		private System.Windows.Forms.DataGridViewTextBoxColumn FieldName;
		private System.Windows.Forms.DataGridViewComboBoxColumn FieldType;
		private System.Windows.Forms.DataGridViewCheckBoxColumn FieldNotNull;
		private System.Windows.Forms.DataGridViewCheckBoxColumn FieldUnique;
		private System.Windows.Forms.DataGridViewTextBoxColumn FieldDefault;
		private System.Windows.Forms.SplitContainer schemaSplitContainer;
		private System.Windows.Forms.TextBox createQueryTextBox;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label currentDatabaseLabel;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label currentQueryLabel;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView tablesDataGridView;
		private System.Windows.Forms.DataGridView settingsDataGridView;
		private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTableName;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRecordsCount;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTableQuery;
		private System.Windows.Forms.ToolStrip queryToolStrip;
		private System.Windows.Forms.ToolStripButton clearQueryToolStripButton;
		private System.Windows.Forms.ToolStripButton executeQueryToolStripButton;
		private ICSharpCode.TextEditor.TextEditorControl queryTextEditorControl;
		private System.Windows.Forms.SplitContainer querySplitContainer;
		private System.Windows.Forms.DataGridView queryGridView;
		private System.Windows.Forms.OpenFileDialog openQueryDialog;
		private System.Windows.Forms.SaveFileDialog saveQueryDialog;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripButton openQueryToolStripButton;
		private System.Windows.Forms.ToolStripButton saveQueryToolStripButton;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem importMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exportMenuItem;
	}
}

