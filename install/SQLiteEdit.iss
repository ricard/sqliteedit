#define AppName "SQLiteEdit"
#define AppVersion GetFileVersion(AddBackslash(SourcePath) + "..\bin\Release\SQLiteEdit.exe")
#define AppPublisher "SoftArchi"

;#define AppVersionNotProtected GetFileVersion(AddBackslash(SourcePath) + "..\bin\Release\SQLiteEdit.exe")
;
;#if AppVersion != AppVersionNotProtected
;# error Not protected !!
;#endif

[Setup]
AppName={#AppName}
AppVersion={#AppVersion}
AppPublisher={#AppPublisher}
AppVerName={#AppName} v{#AppVersion} by {#AppPublisher}
OutputBaseFilename={#AppName}-{#AppVersion}-Setup
DefaultDirName={pf}\{#AppPublisher}\{#AppName}
DefaultGroupName={#AppPublisher}\{#AppName}
ShowLanguageDialog=yes
WizardImageFile=wizard-image.bmp
OutputDir=Output
Compression=lzma
VersionInfoVersion={#AppVersion}
VersionInfoCompany={#AppPublisher}
VersionInfoTextVersion={#AppVersion}
VersionInfoCopyright=Copyright � {#AppPublisher}
UninstallDisplayIcon={app}\SQLiteEdit.exe
AppPublisherURL=http://www.softarchi.com
;LicenseFile=doc\Licence-fr.rtf
AppID={{48959862-592E-4214-8271-A794A5D614EB}

[Files]
Source: ..\bin\Release\System.Data.SQLite.DLL; DestDir: {app}; Flags: overwritereadonly ignoreversion
Source: ..\bin\Release\SQLiteEdit.exe; DestDir: {app}; Flags: overwritereadonly ignoreversion
Source: ..\bin\Release\SQLiteEdit.exe.config; DestDir: {app}; Flags: overwritereadonly ignoreversion
Source: ..\bin\Release\ICSharpCode.TextEditor.dll; DestDir: {app}; Flags: overwritereadonly ignoreversion
Source: ..\Resources\database.ico; DestDir: {app}; Flags: overwritereadonly

[Run]
Filename: {ini:{tmp}\dep.ini,install,windowsInstaller31}; Parameters: /passive /norestart; Description: Windows Installer 3.1; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Windows Installer 3.1""}"
Filename: {ini:{tmp}\dep.ini,install,dotnet20Redist}; Parameters: "/q:a /c:""install /qb!"""; Description: .NET 2.0 Install; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Microsoft .NET 2.0""}"
Filename: {ini:{tmp}\dep.ini,install,dotnet20sdk}; Parameters: "/q:a /c:""install /qb!"""; Description: .NET 2.0 SDK Install; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Microsoft .NET 2.0 SDK""}"
;Filename: {ini:{tmp}\dep.ini,install,vc8redist}; Parameters: ; Description: Visual Studio 8 Redistribuables Install; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Visual Studio 8 Redistribuables""}"
;Filename: {ini:{tmp}\dep.ini,install,anubis}; Parameters: /silent; Description: Anubis Language Install; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Anubis Language""}"
;Filename: {app}\bin\setup\PostInstallTasks.bat; WorkingDir: {app}\bin\setup; StatusMsg: {cm:Compiling}; Flags: runhidden
Filename: {app}\SQLiteEdit.exe; Description: {cm:LaunchProgram,{#AppName}}; Flags: nowait postinstall skipifsilent; WorkingDir: {app}

[INI]
Filename: {app}\softarchi.url; Section: InternetShortcut; Key: URL; String: http://www.softarchi.com/

[UninstallDelete]
Type: files; Name: {app}\softarchi.url

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked
;Name: ResetSettings; Description: {cm:ResetAllSettings}; GroupDescription: Settings:; Flags: unchecked

[Icons]
Name: {group}\{#AppName}; Filename: {app}\SQLiteEdit.exe; WorkingDir: {app}; IconIndex: 0; Comment: {#AppName} {#AppVersion}
Name: {group}\{#AppName} on the Web; Filename: {app}\softarchi.url; WorkingDir: {app}
Name: {userdesktop}\{#AppName}; Filename: {app}\SQLiteEdit.exe; Tasks: desktopicon; WorkingDir: {app}; IconIndex: 0; Comment: {#AppName} {#AppVersion}
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppName}; Filename: {app}\SQLiteEdit.exe; Tasks: quicklaunchicon; WorkingDir: {app}; IconIndex: 0; Comment: {#AppName} {#AppVersion}
Name: {group}\{cm:UninstallProgram, {#AppName}}; Filename: {uninstallexe}


[Languages]
;Name: en; MessagesFile: compiler:Default.isl,{#ISSI_IncludePath}\Languages\_issi_English.isl; LicenseFile: doc\Licence-en.rtf
;Name: fr; MessagesFile: compiler:\Languages\French.isl,{#ISSI_IncludePath}\Languages\_issi_French.isl; LicenseFile: doc\Licence-fr.rtf
Name: en; MessagesFile: compiler:Default.isl
Name: fr; MessagesFile: compiler:\Languages\French.isl

[_ISTool]
UseAbsolutePaths=false
EnableISX=true

[CustomMessages]
en.Dependencies=Dependencies to install:
fr.Dependencies=Logiciels pr�-requis � installer :
en.ResetAllSettings=Reset all user settings
fr.ResetAllSettings=R�initialiser toutes les pr�f�rences utilisateur
en.ExternalSetup=Installing %1... (This may take a few minutes)
fr.ExternalSetup=Installation de %1... (Cela peut prendre quelques minutes)

[Code]
#include "isxdl.iss"

var
  iePath, windowsInstaller31Path, dotnet20RedistPath: string;
  downloadNeeded: boolean;
  hWnd: Integer;
  versionMS, versionLS: Cardinal;

  memoDependenciesNeeded: string;

const
  windowsInstaller31URL = 'http://www.softarchi.com/redist/WindowsInstaller-KB893803-v2-x86.exe';
  dotnet20RedistURL = 'http://download.microsoft.com/download/5/6/7/567758a3-759e-473e-bf8f-52154438565a/dotnetfx.exe';

function InitializeSetup(): Boolean;
var
  sRet: string;
  nRet: integer;

begin
  Result := true;

  // Check for required Windows Installer 3.1 installation
  if (not GetVersionNumbers(ExpandConstant('{sys}') + '\MSI.DLL', versionMS, versionLS)) then
		versionMS := 0;
	if (versionMS < $30000) then begin
    memoDependenciesNeeded := memoDependenciesNeeded + '      Windows Installer 3.1' #13;
    windowsInstaller31Path := ExtractFileDrive(ExpandConstant('{src}')) + '\dependencies\WindowsInstaller-KB893803-v2-x86.exe';
    if not FileExists(windowsInstaller31Path) then begin
      windowsInstaller31Path := ExpandConstant('{tmp}\WindowsInstaller-KB893803-v2-x86.exe');
      if not FileExists(windowsInstaller31Path) then begin
        isxdl_AddFile(windowsInstaller31URL, windowsInstaller31Path);
        downloadNeeded := true;
      end;
    end;
    SetIniString('install', 'windowsInstaller31', windowsInstaller31Path, ExpandConstant('{tmp}\dep.ini'));
  end;

  // Check for required netfx 2.0 installation
  if (not RegKeyExists(HKLM, 'Software\Microsoft\.NETFramework\policy\v2.0')) then begin
    memoDependenciesNeeded := memoDependenciesNeeded + '      .NET Framework 2.0' #13;
    dotnet20RedistPath := ExtractFileDrive(ExpandConstant('{src}')) + '\dependencies\dotnetfx.exe';
    if not FileExists(dotnet20RedistPath) then begin
      dotnet20RedistPath := ExpandConstant('{tmp}\dotnetfx.exe');
      if not FileExists(dotnet20RedistPath) then begin
        isxdl_AddFile(dotnet20RedistURL, dotnet20RedistPath);
        downloadNeeded := true;
      end;
    end;
    SetIniString('install', 'dotnet20Redist', dotnet20RedistPath, ExpandConstant('{tmp}\dep.ini'));
  end;


  sRet := '';
  RegQueryStringValue(HKLM, 'Software\Microsoft\Internet Explorer', 'Version', sRet);
  if downloadNeeded and (sRet < '3') then begin
    // Downloads are needed and isxdl can't initialize to download them, abort
    Result := false;
    if FileExists(iePath) then begin
      MsgBox('Internet Explorer 5 (or later) must be installed manually before Setup can continue.' #13#13 'Setup will now exit and begin the IE 6 installation.', mbInformation, MB_OK);
      ShellExec('open', iePath, '', '', SW_SHOW, ewNoWait, nRet);
    end else begin
      if MsgBox('Internet Explorer 5 (or later) must be installed manually before Setup can continue.' #13#13 'Would you like to visit the download site now?', mbConfirmation, MB_OKCANCEL) = IDOK then
        ShellExec('open', 'http://www.microsoft.com/ie', '', '', SW_SHOW, ewNoWait, nRet);
    end;
  end;
end;


function NextButtonClick(CurPage: Integer): Boolean;
var
  hWnd: Integer;

begin
  Result := true;

  if CurPage = wpReady then begin

    hWnd := StrToInt(ExpandConstant('{wizardhwnd}'));

    // don't try to init isxdl if it's not needed because it will error on < ie 3
    if downloadNeeded then
      if isxdl_DownloadFiles(hWnd) = 0 then Result := false;
  end;
end;

function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo, MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: String): String;
var
  s: string;

begin
  if memoDependenciesNeeded <> '' then s := s + ExpandConstant('{cm:Dependencies}') + NewLine + memoDependenciesNeeded + NewLine;
  if MemoUserInfoInfo <> '' then s := s + MemoUserInfoInfo + NewLine + NewLine;
  if MemoDirInfo <> '' then s := s + MemoDirInfo + NewLine + NewLine;
  if MemoTypeInfo <> '' then s := s + MemoTypeInfo + NewLine + NewLine;
  if MemoComponentsInfo <> '' then s := s + MemoComponentsInfo + NewLine + NewLine;
  if MemoGroupInfo <> '' then s := s + MemoGroupInfo + NewLine + NewLine;
  if MemoTasksInfo <> '' then s := s + MemoTasksInfo + NewLine + NewLine;

  Result := s
end;
[Registry]
Root: HKCR; SubKey: .db3; ValueType: string; ValueData: SQLite3 database file; Flags: uninsdeletekey
Root: HKCR; SubKey: SQLite3 database file; ValueType: string; ValueData: SQLite3 database file; Flags: uninsdeletekey
Root: HKCR; SubKey: SQLite3 database file\Shell\Open\Command; ValueType: string; ValueData: """{app}\SQLiteEdit.exe"" ""%1"""; Flags: uninsdeletevalue
Root: HKCR; Subkey: SQLite3 database file\DefaultIcon; ValueType: string; ValueData: {app}\database.ico; Flags: uninsdeletevalue
