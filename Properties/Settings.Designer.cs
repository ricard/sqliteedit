﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SQLiteEdit.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3." +
            "org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <s" +
            "tring />\r\n</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection dbFiles {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["dbFiles"]));
            }
            set {
                this["dbFiles"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::System.Collections.Specialized.StringCollection queryHistory {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["queryHistory"]));
            }
            set {
                this["queryHistory"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("50, 50")]
        public global::System.Drawing.Point mainFormLocation {
            get {
                return ((global::System.Drawing.Point)(this["mainFormLocation"]));
            }
            set {
                this["mainFormLocation"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("696, 497")]
        public global::System.Drawing.Size mainFormClientSize {
            get {
                return ((global::System.Drawing.Size)(this["mainFormClientSize"]));
            }
            set {
                this["mainFormClientSize"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("352")]
        public int bottomSplitterDistance {
            get {
                return ((int)(this["bottomSplitterDistance"]));
            }
            set {
                this["bottomSplitterDistance"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("231")]
        public int topSplitterDistance {
            get {
                return ((int)(this["topSplitterDistance"]));
            }
            set {
                this["topSplitterDistance"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("225")]
        public int schemaSplitterDistance {
            get {
                return ((int)(this["schemaSplitterDistance"]));
            }
            set {
                this["schemaSplitterDistance"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("110")]
        public int querySplitterDistance {
            get {
                return ((int)(this["querySplitterDistance"]));
            }
            set {
                this["querySplitterDistance"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("query.sql")]
        public string queryFilename {
            get {
                return ((string)(this["queryFilename"]));
            }
            set {
                this["queryFilename"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Normal")]
        public global::System.Windows.Forms.FormWindowState mainFormState {
            get {
                return ((global::System.Windows.Forms.FormWindowState)(this["mainFormState"]));
            }
            set {
                this["mainFormState"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("table.csv")]
        public string importFilename {
            get {
                return ((string)(this["importFilename"]));
            }
            set {
                this["importFilename"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("table.csv")]
        public string exportFilename {
            get {
                return ((string)(this["exportFilename"]));
            }
            set {
                this["exportFilename"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(";")]
        public string csvSeparator {
            get {
                return ((string)(this["csvSeparator"]));
            }
            set {
                this["csvSeparator"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool csvUseStringDelimiter {
            get {
                return ((bool)(this["csvUseStringDelimiter"]));
            }
            set {
                this["csvUseStringDelimiter"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\"")]
        public string csvStringDelimiter {
            get {
                return ((string)(this["csvStringDelimiter"]));
            }
            set {
                this["csvStringDelimiter"] = value;
            }
        }
    }
}
