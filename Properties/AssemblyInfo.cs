﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SQLiteEdit")]
[assembly: AssemblyDescription("SQLiteEdit is a powerfull but very easy to use database manager for SQLite3.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SoftArchi")]
[assembly: AssemblyProduct("SQLiteEdit")]
[assembly: AssemblyCopyright("Copyright © SoftArchi 2006-2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b29af984-4ebf-4358-8fab-fcd2b00c6ed7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.5.3.12")]
[assembly: AssemblyFileVersion("1.5.3.12")]
